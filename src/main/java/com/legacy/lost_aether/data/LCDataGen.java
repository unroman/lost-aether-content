package com.legacy.lost_aether.data;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.legacy.lost_aether.LostContentMod;
import com.legacy.structure_gel.api.data.providers.NestedDataProvider;
import com.legacy.structure_gel.api.data.providers.RegistrarDatapackEntriesProvider;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.DetectedVersion;
import net.minecraft.core.HolderLookup;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.PackOutput;
import net.minecraft.data.metadata.PackMetadataGenerator;
import net.minecraft.network.chat.Component;
import net.minecraft.server.packs.PackType;
import net.minecraft.server.packs.metadata.pack.PackMetadataSection;
import net.minecraftforge.common.data.BlockTagsProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.data.event.GatherDataEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class LCDataGen
{
	@SubscribeEvent
	public static void gatherData(GatherDataEvent event)
	{
		DataGenerator gen = event.getGenerator();
		ExistingFileHelper helper = event.getExistingFileHelper();
		PackOutput output = gen.getPackOutput();
		boolean server = event.includeServer();

		RegistrarDatapackEntriesProvider provider = RegistrarHandler.createGenerator(gen.getPackOutput(), LostContentMod.MODID);
		CompletableFuture<HolderLookup.Provider> lookup = provider.getLookupProvider();
		gen.addProvider(server, provider);

		BlockTagsProvider blockTagProv = new LCTagProv.BlockTagProv(gen, helper, lookup);
		gen.addProvider(server, blockTagProv);
		gen.addProvider(server, new LCTagProv.ItemProv(gen, blockTagProv.contentsGetter(), helper, lookup));
		gen.addProvider(server, new LCTagProv.EntityProv(gen, helper, lookup));
		gen.addProvider(server, new LCTagProv.BiomeProv(gen, helper, lookup));
		gen.addProvider(server, new LCTagProv.StructureProv(gen, helper, lookup));

		gen.addProvider(server, new LCLootProv(gen, helper, lookup));

		gen.addProvider(server, new LCRecipeProv(output));
		gen.addProvider(server, new LCAdvancementProv(gen, lookup, helper));

		gen.addProvider(server, new LCModelProv.States(output, helper));
		/*gen.addProvider(server, new LCModelProv.Blocks(output, helper));*/
		gen.addProvider(server, new LCModelProv.Items(output, helper));

		gen.addProvider(server, new LCLootModifiers(output));

		gen.addProvider(server, packMcmeta(output, "Lost Aether Content resources"));

		PackOutput legacyPackOutput = gen.getPackOutput("assets/lost_aether_content/classic_pack");
		gen.addProvider(server, packMcmeta(legacyPackOutput, "Old textures for Aether: Lost Content (optional)"));

		/*gen.addProvider(server, new LCSoundProv(output, helper));*/
	}
	
	private static final NestedDataProvider<PackMetadataGenerator> packMcmeta(PackOutput output, String description)
	{
		Map<PackType, Integer> packVersionMap = Stream.of(PackType.values()).collect(Collectors.toMap(Function.identity(), DetectedVersion.BUILT_IN::getPackVersion));
		int packVersion = packVersionMap.get(PackType.SERVER_DATA);
		return NestedDataProvider.of(new PackMetadataGenerator(output).add(PackMetadataSection.TYPE, new PackMetadataSection(Component.literal(description), packVersion, packVersionMap)), description);
	}
}
