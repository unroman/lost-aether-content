package com.legacy.lost_aether.data;

import java.util.function.Consumer;

import com.aetherteam.aether.block.AetherBlocks;
import com.aetherteam.aether.data.providers.AetherRecipeProvider;
import com.aetherteam.aether.entity.AetherEntityTypes;
import com.aetherteam.aether.item.AetherItems;
import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.registry.LCBlocks;
import com.legacy.lost_aether.registry.LCItems;
import com.legacy.lost_aether.registry.LCMoaTypes;

import net.minecraft.data.PackOutput;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.data.recipes.ShapedRecipeBuilder;
import net.minecraftforge.common.Tags;

public class LCRecipeProv extends AetherRecipeProvider
{
	public LCRecipeProv(PackOutput output)
	{
		super(output, LostContentMod.MODID);
	}

	@Override
	protected void buildRecipes(Consumer<FinishedRecipe> consumer)
	{
		stonecuttingRecipe(consumer, RecipeCategory.DECORATIONS, LCBlocks.gale_wall, LCBlocks.gale_stone);
		stonecuttingRecipe(consumer, RecipeCategory.BUILDING_BLOCKS, LCBlocks.gale_stairs, LCBlocks.gale_stone);
		stonecuttingRecipe(consumer, RecipeCategory.BUILDING_BLOCKS, LCBlocks.gale_slab, LCBlocks.gale_stone, 2);
		stonecuttingRecipe(consumer, RecipeCategory.BUILDING_BLOCKS, LCBlocks.gale_pillar, LCBlocks.gale_stone);
		stonecuttingRecipe(consumer, RecipeCategory.BUILDING_BLOCKS, LCBlocks.gale_stone, LCBlocks.gale_pillar);

		stonecuttingRecipe(consumer, RecipeCategory.BUILDING_BLOCKS, LCBlocks.light_gale_stone, LCBlocks.gale_stone);
		stonecuttingRecipe(consumer, RecipeCategory.BUILDING_BLOCKS, LCBlocks.gale_stone, LCBlocks.light_gale_stone);

		repairingRecipe(RecipeCategory.COMBAT, LCItems.zanite_shield, 2250).group("altar_shield_repair").save(consumer, name("zanite_shield_repairing"));
		repairingRecipe(RecipeCategory.COMBAT, LCItems.gravitite_shield, 5500).group("altar_shield_repair").save(consumer, name("gravitite_shield_repairing"));

		enchantingRecipe(RecipeCategory.BUILDING_BLOCKS, LCBlocks.enchanted_pink_aercloud, LCBlocks.pink_aercloud, 0.3F, 30 * 20).save(consumer, name("enchanted_pink_aercloud_enchanting"));

		moaIncubationRecipe(AetherEntityTypes.MOA.get(), LCMoaTypes.BROWN, LCItems.brown_moa_egg).save(consumer, name("brown_moa_incubation"));

		// @formatter:off
		 ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, LCItems.zanite_shield, 1)
         .group("minecraft:shield")
         .define('G', AetherItems.ZANITE_GEMSTONE.get())
         .define('R', Tags.Items.RODS_WOODEN)
         .pattern("GRG")
         .pattern("GGG")
         .pattern(" G ")
         .unlockedBy("has_zanite_gemstone", has(AetherItems.ZANITE_GEMSTONE.get()))
         .save(consumer, name("zanite_shield"));
		 
		 ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, LCItems.gravitite_shield, 1)
         .group("minecraft:shield")
         .define('G', AetherBlocks.ENCHANTED_GRAVITITE.get())
         .define('R', Tags.Items.RODS_WOODEN)
         .pattern("GRG")
         .pattern("GGG")
         .pattern(" G ")
         .unlockedBy("has_gravitite", has(AetherBlocks.ENCHANTED_GRAVITITE.get()))
         .save(consumer, name("gravitite_shield"));
		// @formatter:on
	}
}
