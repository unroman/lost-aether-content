package com.legacy.lost_aether.data;

import java.util.List;
import java.util.function.Function;

import com.aetherteam.aether.Aether;
import com.aetherteam.aether.block.AetherBlocks;
import com.aetherteam.aether.block.dungeon.DoorwayBlock;
import com.aetherteam.aether.data.providers.AetherBlockStateProvider;
import com.aetherteam.aether.data.providers.AetherItemModelProvider;
import com.aetherteam.aether.item.miscellaneous.MoaEggItem;
import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.item.util.CustomHeadArmorItem;
import com.legacy.lost_aether.registry.LCBlocks;
import com.legacy.lost_aether.registry.LCItems;

import net.minecraft.core.Direction;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ShieldItem;
import net.minecraft.world.item.TieredItem;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.WallBlock;
import net.minecraftforge.client.model.generators.ConfiguredModel;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.client.model.generators.loaders.ItemLayerModelBuilder;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.ForgeRegistries;

// class of nightmares
public class LCModelProv
{
	protected static List<Block> getAllBlocks()
	{
		return ForgeRegistries.BLOCKS.getValues().stream().filter(block -> ForgeRegistries.BLOCKS.getKey(block).getNamespace().equals(LostContentMod.MODID)).toList();
	}

	protected static List<Item> getAllItems(Function<Item, Boolean> condition)
	{
		return ForgeRegistries.ITEMS.getValues().stream().filter(item -> ForgeRegistries.ITEMS.getKey(item).getNamespace().equals(LostContentMod.MODID) && condition.apply(item)).toList();
	}

	protected static List<Item> getAllItems()
	{
		return getAllItems(i -> true);
	}

	public static class Items extends AetherItemModelProvider
	{
		public Items(PackOutput output, ExistingFileHelper existingFileHelper)
		{
			super(output, LostContentMod.MODID, existingFileHelper);
		}

		@Override
		protected void registerModels()
		{
			this.itemBlock(LCBlocks.gale_stone);
			this.itemBlock(LCBlocks.light_gale_stone);
			this.itemLockedDungeonBlock(LCBlocks.locked_gale_stone, LCBlocks.gale_stone);
			this.itemLockedDungeonBlock(LCBlocks.locked_light_gale_stone, LCBlocks.light_gale_stone);

			this.itemBossDoorwayDungeonBlock(LCBlocks.boss_doorway_gale_stone, LCBlocks.gale_stone);
			this.itemBossDoorwayDungeonBlock(LCBlocks.boss_doorway_light_gale_stone, LCBlocks.light_gale_stone);

			this.itemTreasureDoorwayDungeonBlock(LCBlocks.treasure_doorway_gale_stone, LCBlocks.gale_stone);
			this.itemTreasureDoorwayDungeonBlock(LCBlocks.treasure_doorway_light_gale_stone, LCBlocks.light_gale_stone);

			this.itemTrappedDungeonBlock(LCBlocks.trapped_gale_stone, LCBlocks.gale_stone);
			this.itemTrappedDungeonBlock(LCBlocks.trapped_light_gale_stone, LCBlocks.light_gale_stone);

			this.itemBlock(LCBlocks.songstone);
			this.itemBlock(LCBlocks.gale_pillar);
			this.itemBlock(LCBlocks.gale_stairs);
			this.itemBlock(LCBlocks.gale_slab);

			this.itemBlock(LCBlocks.pink_aercloud);
			this.itemBlock(LCBlocks.enchanted_pink_aercloud);

			this.itemWallBlock((WallBlock) LCBlocks.gale_wall, LCBlocks.gale_stone, "dungeon/");

			this.itemBlockFlat(LCBlocks.crystal_sapling, "");
			this.itemBlockFlat(LCBlocks.holiday_sapling, "");

			getAllItems(i -> !(i instanceof BlockItem)).forEach(item ->
			{
				if (item instanceof TieredItem)
					handheldItem(item, "");
				else if (item instanceof MoaEggItem)
					this.moaEggItem(item, "miscellaneous/");
				else if (!(item instanceof ShieldItem || item instanceof CustomHeadArmorItem || item == LCItems.sentry_shield))
					basicItem(item);
			});

			this.rotatedItem(LCItems.platinum_key, "");

			this.withExistingParent(this.itemName(LCItems.sentry_shield), "forge:item/default").texture("layer0", this.itemTexture(this.itemName(LCItems.sentry_shield))).texture("layer1", this.itemTexture(this.itemName(LCItems.sentry_shield) + "_glow")).customLoader(ItemLayerModelBuilder::begin).emissive(15, 15, 1).end();
		}

		@Override
		public void itemBossDoorwayDungeonBlock(Block block, Block baseBlock)
		{
			this.withExistingParent(this.blockName(block), new ResourceLocation(Aether.MODID, this.blockName(AetherBlocks.BOSS_DOORWAY_CARVED_STONE.get()))).texture("face", this.texture(this.blockName(baseBlock), "dungeon/"));
		}

		@Override
		public void itemLockedDungeonBlock(Block block, Block baseBlock)
		{
			this.withExistingParent(this.blockName(block), new ResourceLocation(Aether.MODID, this.blockName(AetherBlocks.LOCKED_CARVED_STONE.get()))).texture("face", this.texture(this.blockName(baseBlock), "dungeon/"));
		}

		@Override
		public void itemTrappedDungeonBlock(Block block, Block baseBlock)
		{
			this.withExistingParent(this.blockName(block), new ResourceLocation(Aether.MODID, this.blockName(AetherBlocks.TRAPPED_CARVED_STONE.get()))).texture("face", this.texture(this.blockName(baseBlock), "dungeon/"));
		}

		@Override
		public void itemTreasureDoorwayDungeonBlock(Block block, Block baseBlock)
		{
			this.withExistingParent(this.blockName(block), new ResourceLocation(Aether.MODID, this.blockName(AetherBlocks.TREASURE_DOORWAY_CARVED_STONE.get()))).texture("face", this.texture(this.blockName(baseBlock), "dungeon/"));
		}

		public void moaEggItem(Item item, String location)
		{
			this.withExistingParent(this.itemName(item), this.mcLoc("item/generated")).texture("layer0", new ResourceLocation(Aether.MODID, "item/" + location + "moa_egg")).texture("layer1", new ResourceLocation(Aether.MODID, "item/" + location + "moa_egg_spot"));
		}

		protected ResourceLocation aeTexture(String name, String location)
		{
			return new ResourceLocation(Aether.MODID, "block/" + location + name);
		}

		protected ResourceLocation itemTexture(String name)
		{
			return this.modLoc("item/" + name);
		}
	}

	public static class States extends AetherBlockStateProvider
	{
		public States(PackOutput output, ExistingFileHelper existingFileHelper)
		{
			super(output, LostContentMod.MODID, existingFileHelper);
		}

		@Override
		protected void registerStatesAndModels()
		{
			Block gale = LCBlocks.gale_stone;
			Block lightGale = LCBlocks.light_gale_stone;

			this.blockTop(gale, "dungeon/");

			var lightBlock = models().withExistingParent(name(lightGale), this.mcLoc("block/block")).renderType("cutout");

			var mainElement = lightBlock.element().from(0, 0, 0).to(16, 16, 16);
			for (Direction dir : Direction.values())
				mainElement.face(dir).texture(dir == Direction.UP ? "#top" : "#all").uvs(0, 0, 16, 16).cullface(dir).end();

			var glowElement = lightBlock.element().from(0, 0, 0).to(16, 16, 16);
			for (Direction dir : Direction.values())
				glowElement.face(dir).texture(dir == Direction.UP ? "#top_glow" : "#glow").emissivity(15, 15).uvs(0, 0, 16, 16).cullface(dir).end();

			var lightTex = this.texture(this.name(lightGale), "dungeon/");
			this.simpleBlock(lightGale, lightBlock.texture("all", lightTex).texture("glow", this.extend(lightTex, "_glow")).texture("top", this.extend(lightTex, "_top")).texture("top_glow", this.extend(lightTex, "_top_glow")).texture("particle", "#all"));

			this.dungeonBlock(LCBlocks.locked_gale_stone, gale);
			this.dungeonBlock(LCBlocks.locked_light_gale_stone, lightGale);

			this.dungeonBlock(LCBlocks.trapped_gale_stone, gale);
			this.dungeonBlock(LCBlocks.trapped_light_gale_stone, lightGale);

			this.dungeonBlock(LCBlocks.treasure_doorway_gale_stone, gale);
			this.dungeonBlock(LCBlocks.treasure_doorway_light_gale_stone, lightGale);

			this.invisibleBlock(LCBlocks.boss_doorway_gale_stone, gale);
			this.invisibleBlock(LCBlocks.boss_doorway_light_gale_stone, lightGale);

			this.pillar((RotatedPillarBlock) LCBlocks.gale_pillar);

			this.translucentBlock(LCBlocks.pink_aercloud, "");
			this.translucentBlock(LCBlocks.enchanted_pink_aercloud, "");

			String dungeonDir = "dungeon/";

			this.stairsBlock((StairBlock) LCBlocks.gale_stairs, this.texture(this.name(gale), dungeonDir), this.texture(this.name(gale), dungeonDir), this.texture(this.name(gale) + "_top", dungeonDir));
			this.slab((SlabBlock) LCBlocks.gale_slab, gale, dungeonDir);
			this.wallBlock((WallBlock) LCBlocks.gale_wall, gale, dungeonDir);

			Block songstoneBlock = LCBlocks.songstone;
			var songstoneTexture = this.texture("songstone", "dungeon/");
			var songstoneModel = models().withExistingParent(name(songstoneBlock), this.mcLoc("block/block")).renderType("cutout").texture("bottom", this.texture(this.name(gale) + "_top", "dungeon/")).texture("top", this.extend(songstoneTexture, "_top")).texture("side", this.extend(songstoneTexture, "_side")).texture("particle", this.extend(songstoneTexture, "_side"));

			var e1 = songstoneModel.element().from(0, 0, 0).to(16, 16, 16);

			for (Direction dir : Direction.values())
			{
				String name = dir == Direction.UP ? "top" : dir == Direction.DOWN ? "bottom" : "side";
				e1.face(dir).texture("#" + name).uvs(0, 0, 16, 16).cullface(dir).end();
			}

			var e2 = songstoneModel.element().from(0, 0, 0).to(16, 16, 16);

			for (Direction dir : Direction.values())
			{
				if (dir != Direction.DOWN)
				{
					String name = dir == Direction.UP ? "top_glow" : "side_glow";
					e2.face(dir).texture("#" + name).emissivity(15, 15).uvs(0, 0, 16, 16).cullface(dir).end();
				}
			}

			this.simpleBlock(songstoneBlock, songstoneModel.texture("side_glow", this.extend(songstoneTexture, "_side_glow")).texture("top_glow", this.extend(songstoneTexture, "_top_glow")));

			this.saplingBlock(LCBlocks.crystal_sapling, "");
			this.saplingBlock(LCBlocks.holiday_sapling, "");
			this.pottedPlant(LCBlocks.potted_crystal_sapling, LCBlocks.crystal_sapling, "");
			this.pottedPlant(LCBlocks.potted_holiday_sapling, LCBlocks.holiday_sapling, "");

			// fallback this.simpleBlock(block)
			getAllBlocks().stream().filter(b -> !this.registeredBlocks.containsKey(b)).forEach(block -> System.err.println("Forgot " + block));
		}

		@Override
		public void dungeonBlock(Block block, Block baseBlock)
		{
			ConfiguredModel dungeonBlock = new ConfiguredModel(this.models().withExistingParent(this.name(block), LostContentMod.locate(this.name(baseBlock))));
			this.getVariantBuilder(block).partialState().setModels(new ConfiguredModel[] { dungeonBlock });
		}

		@Override
		public void invisibleBlock(Block block, Block baseBlock)
		{
			ModelFile visible = this.models().withExistingParent(this.name(block), LostContentMod.locate(this.name(baseBlock)));
			ModelFile invisible = this.models().getBuilder(this.name(block));
			getVariantBuilder(block).forAllStatesExcept(state -> ConfiguredModel.builder().modelFile(!state.getValue(DoorwayBlock.INVISIBLE) ? visible : invisible).build());
		}

		public void blockTop(Block block, String location)
		{
			String dir = "dungeon/";
			simpleBlock(block, models().cubeTop(this.name(block), this.texture(this.name(block), dir), this.texture(this.name(block) + "_top", dir)));
		}
	}

}
