package com.legacy.lost_aether.client.models;

import com.legacy.lost_aether.entity.AerwhaleKingEntity;

import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.util.Mth;

public class AerwhaleKingModel<T extends AerwhaleKingEntity> extends HierarchicalModel<T>
{
	private final ModelPart root;

	private final ModelPart head, middleBody, backBody;
	private final ModelPart rightFin, leftFin, middleFin, backRightFin, backLeftFin;

	public AerwhaleKingModel(ModelPart root)
	{
		this.root = root;

		this.head = root.getChild("head");
		this.rightFin = this.head.getChild("right_fin");
		this.leftFin = this.head.getChild("left_fin");

		this.middleBody = this.head.getChild("middle_body");
		this.middleFin = this.middleBody.getChild("middle_fin");

		this.backBody = this.middleBody.getChild("back_body");
		this.backRightFin = this.backBody.getChild("back_right_fin");
		this.backLeftFin = this.backBody.getChild("back_left_fin");
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition mesh = new MeshDefinition();
		PartDefinition root = mesh.getRoot();

		var head = root.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 0).addBox(-12.0F, -9.0F, -14.0F, 24.0F, 18.0F, 28.0F), PartPose.offset(0.0F, 14.0F, 0.0F));
		head.addOrReplaceChild("head_belly", CubeListBuilder.create().texOffs(0, 51).addBox(-13.0F, 4.0F, -15.0F, 26.0F, 6.0F, 30.0F), PartPose.offset(0.0F, 0.0F, 0.0F));
		head.addOrReplaceChild("crown", CubeListBuilder.create().texOffs(100, 58).addBox(-4.0F, -5.0F, -4.0F, 8.0F, 5.0F, 8.0F), PartPose.offset(0.0F, -9.0F, 3.0F));
		head.addOrReplaceChild("right_fin", CubeListBuilder.create().texOffs(76, 0).mirror().addBox(-20.0F, -1.5F, -7.0F, 19.0F, 3.0F, 14.0F).mirror(false), PartPose.offset(-10.0F, 3.5F, 11.0F));
		head.addOrReplaceChild("left_fin", CubeListBuilder.create().texOffs(76, 0).addBox(1.5F, -1.5F, -7.0F, 19.0F, 3.0F, 14.0F), PartPose.offset(9.5F, 3.5F, 11.0F));

		var middleBody = head.addOrReplaceChild("middle_body", CubeListBuilder.create().texOffs(134, 0).addBox(-11.0F, -5.0F, -1.0F, 22.0F, 14.0F, 25.0F), PartPose.offset(0.0F, -1.0F, 14.0F));
		middleBody.addOrReplaceChild("middle_belly", CubeListBuilder.create().texOffs(0, 98).addBox(-12.0F, 5.0F, -15.0F, 24.0F, 6.0F, 26.0F), PartPose.offset(0.0F, -1.0F, 14.0F));
		middleBody.addOrReplaceChild("middle_fin", CubeListBuilder.create().texOffs(110, 24).addBox(-1.0F, -3.5F, -4.0F, 2.0F, 7.0F, 8.0F), PartPose.offset(0.0F, -6.5F, 12.0F));

		var backBody = middleBody.addOrReplaceChild("back_body", CubeListBuilder.create().texOffs(149, 96).addBox(-10.5F, -8.3F, -1.5F, 17.0F, 10.0F, 22.0F), PartPose.offset(2.0F, 4.0F, 24.0F));
		backBody.addOrReplaceChild("back_belly", CubeListBuilder.create().texOffs(0, 141).addBox(-11.5F, -1.6F, 13.7F, 19.0F, 5.0F, 21.0F), PartPose.offset(0.0F, 2.3F, -13.2F));
		backBody.addOrReplaceChild("back_right_fin", CubeListBuilder.create().texOffs(150, 53).mirror().addBox(-10.0F, -1.5F, -4.0F, 15.0F, 3.0F, 24.0F).mirror(false), PartPose.offset(-7.0F, 1.0F, 19.0F));
		backBody.addOrReplaceChild("back_left_fin", CubeListBuilder.create().texOffs(150, 53).addBox(-5.0F, -1.5F, -4.0F, 15.0F, 3.0F, 24.0F), PartPose.offset(3.0F, 1.0F, 19.0F));

		return LayerDefinition.create(mesh, 228, 200);
	}

	@Override
	public ModelPart root()
	{
		return this.root;
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		this.head.resetPose();
		
		this.head.yScale += entity.getStandingAnimationScale(ageInTicks - entity.tickCount) * 0.2F;
		this.middleBody.resetPose();
		
		
		this.head.xRot = headPitch / 57.29578F;
		this.head.yRot = netHeadYaw / 57.29578F;

		boolean faster = entity.isAggressive() && !entity.isShooting();
		float speed = entity.hasTargetPos() ? 0.8F : faster ? 0.2F : 0.14F;
		this.leftFin.zRot = (Mth.cos((ageInTicks - 20) * speed) * 0.3F);
		this.rightFin.zRot = (Mth.cos((ageInTicks - 20) * speed) * -0.3F);

		// fins
		this.leftFin.yRot = -0.20943951023931953F;
		this.rightFin.yRot = 0.20943951023931953F;
		this.backLeftFin.yRot = 0.7330382858376184F;
		this.backRightFin.yRot = -0.7330382858376184F;

		if (entity.hasTargetPos())
		{
			this.leftFin.yRot += -0.7F;
			this.rightFin.yRot += 0.7F;

			this.backLeftFin.yRot += -0.2F;
			this.backRightFin.yRot += 0.2F;
		}

		float mul = !entity.isStunned() ? (entity.hasTargetPos() ? 0.5F : faster ? 0.45F : 0.35F) : 0;

		this.middleFin.xRot = -0.3F + (Mth.sin(ageInTicks * 0.1F) * 0.1F);
		this.middleFin.zRot = Mth.cos(ageInTicks * 0.1F) * 0.05F;

		this.head.xRot += (Mth.cos(ageInTicks * speed) * mul) / 4;
		this.middleBody.xRot = (Mth.cos(20 - ageInTicks * speed) * mul) / 3;
		this.backBody.xRot = (Mth.cos(40 - ageInTicks * speed) * mul) / 2;

		this.middleBody.yRot = 0;
		this.backBody.yRot = 0;

		float finMove = (Mth.cos(60 - ageInTicks * speed) * mul) / 2;
		this.backLeftFin.xRot = finMove;
		this.backRightFin.xRot = finMove;

		this.backLeftFin.zRot = finMove;
		this.backRightFin.zRot = -finMove;

		if (entity.isStunned())
		{
			speed = 0.50F;
			mul = 0.05F;

			this.leftFin.zRot = (Mth.sin((ageInTicks - 20) * speed) * 0.3F);
			this.rightFin.zRot = (Mth.sin((ageInTicks - 20) * speed) * -0.3F);

			this.head.xRot += (Mth.cos(ageInTicks * speed) * mul) * 0.5F;
			this.head.yRot += (Mth.sin(ageInTicks * speed) * mul) * 2;

			this.middleBody.xRot = (Mth.cos(20 - ageInTicks * speed) * mul) * 0.5F;
			this.middleBody.yRot = (Mth.sin(20 - ageInTicks * speed * 2) * mul) * 2;

			this.backBody.xRot = (Mth.cos(40 - ageInTicks * speed) * mul);
			this.backBody.yRot = (Mth.sin(40 - ageInTicks * speed * 2) * mul) * 2;
		}

		if (entity.isShooting())
		{
			speed = 0.7F;
			mul = 0.20F;
			this.head.xRot += 0.2F + (Mth.cos(ageInTicks * speed) * mul) / 4;
			this.middleBody.xRot = -0.5F + (Mth.cos(20 - ageInTicks * speed) * mul) / 3;
			this.backBody.xRot = -0.3F + (Mth.cos(40 - ageInTicks * speed) * mul) / 2;

			this.leftFin.zRot = (Mth.cos((ageInTicks - 20) * speed) * 0.3F);
			this.rightFin.zRot = (Mth.cos((ageInTicks - 20) * speed) * -0.3F);

			finMove = (Mth.cos(60 - ageInTicks * speed) * mul) / 2;
			this.backLeftFin.xRot = finMove;
			this.backRightFin.xRot = finMove;

			this.backLeftFin.zRot = finMove;
			this.backRightFin.zRot = -finMove;
		}

		/*this.head.xRot += -Mth.lerp(ageInTicks - entity.tickCount, entity.yAngleO, entity.yAngle);*/
	}

}