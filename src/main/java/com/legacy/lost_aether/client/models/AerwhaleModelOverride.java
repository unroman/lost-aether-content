package com.legacy.lost_aether.client.models;

import javax.annotation.Nonnull;

import com.aetherteam.aether.client.renderer.entity.model.AerwhaleModel;
import com.aetherteam.aether.entity.passive.Aerwhale;
import com.legacy.lost_aether.LostContentConfig;

import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.util.Mth;

public interface AerwhaleModelOverride
{
	// AerwhaleModel
	public static void setupAnim(@Nonnull Aerwhale entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, ModelPart head)
	{
		// just cancel it if we're missing things somehow
		if (!head.hasChild("middle_body") && !head.hasChild("back_body"))
			return;

		head.z = -20;
		var rightFin = head.getChild("right_fin");
		var leftFin = head.getChild("left_fin");

		var middleBody = head.getChild("middle_body");
		var middleFin = middleBody.getChild("middle_fin");

		var backBody = middleBody.getChild("back_body");
		var backRightFin = backBody.getChild("back_right_fin");
		var backLeftFin = backBody.getChild("back_left_fin");

		float speed = 0.1F;
		leftFin.zRot = (Mth.cos((ageInTicks - 20) * speed) * 0.3F);
		rightFin.zRot = (Mth.cos((ageInTicks - 20) * speed) * -0.3F);

		// fins
		leftFin.yRot = -0.20943951023931953F;
		rightFin.yRot = 0.20943951023931953F;
		backLeftFin.yRot = 0.7330382858376184F;
		backRightFin.yRot = -0.7330382858376184F;

		float mul = 0.30F;

		middleFin.xRot = -0.3F + (Mth.sin(ageInTicks * 0.1F) * 0.1F);
		middleFin.zRot = Mth.cos(ageInTicks * 0.1F) * 0.05F;

		head.xRot = (Mth.cos(ageInTicks * speed) * mul) / 4;
		middleBody.xRot = (Mth.cos(20 - ageInTicks * speed) * mul) / 3;
		backBody.xRot = (Mth.cos(40 - ageInTicks * speed) * mul) / 2;

		middleBody.yRot = 0;
		backBody.yRot = 0;

		float finMove = (Mth.cos(60 - ageInTicks * speed) * mul) / 2;
		backLeftFin.xRot = finMove;
		backRightFin.xRot = finMove;

		backLeftFin.zRot = finMove;
		backRightFin.zRot = -finMove;
	}

	public static LayerDefinition createOverrideLayer()
	{
		if (!LostContentConfig.CLIENT.updatedAerwhaleAnimations.get())
			return AerwhaleModel.createBodyLayer();

		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition head = partdefinition.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 0).addBox(-12.0F, -9.0F, -14.0F, 24.0F, 18.0F, 28.0F), PartPose.offset(0.0F, 14.0F, 0.0F));
		head.addOrReplaceChild("head_belly", CubeListBuilder.create().texOffs(104, 0).addBox(-13.0F, 4.0F, -15.0F, 26.0F, 6.0F, 30.0F), PartPose.offset(0.0F, 0.0F, 0.0F));
		head.addOrReplaceChild("right_fin", CubeListBuilder.create().texOffs(104, 94).mirror().addBox(-20.0F, -1.5F, -7.0F, 19.0F, 3.0F, 14.0F).mirror(false), PartPose.offset(-10.0F, 3.5F, 11.0F));
		head.addOrReplaceChild("left_fin", CubeListBuilder.create().texOffs(104, 94).addBox(1.5F, -1.5F, -7.0F, 19.0F, 3.0F, 14.0F), PartPose.offset(9.5F, 3.5F, 11.0F));

		PartDefinition bodyMiddle = head.addOrReplaceChild("middle_body", CubeListBuilder.create().texOffs(0, 46).addBox(-11.0F, -5.0F, -1.0F, 22.0F, 14.0F, 25.0F), PartPose.offset(0.0F, -1.0F, 14.0F));
		bodyMiddle.addOrReplaceChild("middle_belly", CubeListBuilder.create().texOffs(104, 36).addBox(-12.0F, 5.0F, -15.0F, 24.0F, 6.0F, 26.0F), PartPose.offset(0.0F, -1.0F, 14.0F));
		bodyMiddle.addOrReplaceChild("middle_fin", CubeListBuilder.create().texOffs(0, 46).addBox(-1.0F, -3.5F, -4.0F, 2.0F, 7.0F, 8.0F), PartPose.offset(0.0F, -6.5F, 12.0F));

		PartDefinition bodyBack = bodyMiddle.addOrReplaceChild("back_body", CubeListBuilder.create().texOffs(0, 85).addBox(-10.5F, -8.3F, -1.5F, 17.0F, 10.0F, 22.0F), PartPose.offset(2.0F, 4.0F, 24.0F));
		bodyBack.addOrReplaceChild("back_belly", CubeListBuilder.create().texOffs(104, 68).addBox(-11.5F, -1.6F, 13.7F, 19.0F, 5.0F, 21.0F), PartPose.offset(0.0F, 2.3F, -13.2F));

		bodyBack.addOrReplaceChild("back_right_fin", CubeListBuilder.create().texOffs(170, 84).mirror().addBox(-10.0F, -1.5F, -4.0F, 15.0F, 3.0F, 24.0F).mirror(false), PartPose.offset(-7.0F, 1.0F, 19.0F));
		bodyBack.addOrReplaceChild("back_left_fin", CubeListBuilder.create().texOffs(170, 84).addBox(-5.0F, -1.5F, -4.0F, 15.0F, 3.0F, 24.0F), PartPose.offset(3.0F, 1.0F, 19.0F));

		return LayerDefinition.create(meshdefinition, 256, 128);
	}
}
