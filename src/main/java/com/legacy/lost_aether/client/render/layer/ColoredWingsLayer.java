package com.legacy.lost_aether.client.render.layer;

import javax.annotation.Nonnull;

import com.aetherteam.aether.client.renderer.entity.model.QuadrupedWingsModel;
import com.aetherteam.aether.entity.passive.WingedAnimal;
import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.capability.entity.IWingedAnimal;
import com.legacy.lost_aether.capability.entity.WingedAnimalCap;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.model.QuadrupedModel;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.RenderLayerParent;
import net.minecraft.client.renderer.entity.layers.RenderLayer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;

public class ColoredWingsLayer<T extends WingedAnimal, M extends QuadrupedModel<T>> extends RenderLayer<T, M>
{
	private static final ResourceLocation BRONZE_WINGS = LostContentMod.locate("textures/entity/bronze_wings.png");
	private static final ResourceLocation SILVER_WINGS = LostContentMod.locate("textures/entity/silver_wings.png");

	private final RenderType bronzeType, silverType;
	private final QuadrupedWingsModel<T> wingModel;

	public ColoredWingsLayer(RenderLayerParent<T, M> entityRenderer, QuadrupedWingsModel<T> wingsModel)
	{
		super(entityRenderer);

		this.wingModel = wingsModel;
		this.bronzeType = RenderType.entityCutoutNoCull(BRONZE_WINGS);
		this.silverType = RenderType.entityCutoutNoCull(SILVER_WINGS);
	}

	@Override
	public void render(@Nonnull PoseStack poseStack, @Nonnull MultiBufferSource buffer, int packedLight, T entity, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch)
	{
		IWingedAnimal cap = WingedAnimalCap.get(entity);

		if (cap != null && cap.shouldDisplayWings())
		{
			int type = cap.getWingType();

			if (entity.isBaby())
			{
				poseStack.scale(0.5F, 0.5F, 0.5F);
				poseStack.translate(0.0F, 1.5F, 0.0F);
			}

			this.wingModel.setupAnim(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
			VertexConsumer consumer = buffer.getBuffer(type == WingedAnimalCap.WingType.SILVER.ordinal() ? this.silverType : this.bronzeType);
			this.wingModel.renderToBuffer(poseStack, consumer, packedLight, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
		}
	}
}
