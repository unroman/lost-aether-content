package com.legacy.lost_aether.client.render;

import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.client.LCRenderRefs;
import com.legacy.lost_aether.client.models.ZephyrooModel;
import com.legacy.lost_aether.entity.ZephyrooEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
@Deprecated
public class ZephyrooRenderer<T extends ZephyrooEntity> extends MobRenderer<T, ZephyrooModel<T>>
{
	private static final ResourceLocation TEXTURE = LostContentMod.locate("textures/entity/zephyroo/zephyroo.png");

	public ZephyrooRenderer(EntityRendererProvider.Context context)
	{
		super(context, new ZephyrooModel<>(context.bakeLayer(LCRenderRefs.ZEPHYROO)), 0.5F);
	}

	@Override
	protected void scale(T entityIn, PoseStack matrixStackIn, float partialTicks)
	{
		if (entityIn.isBaby())
			matrixStackIn.scale(0.5F, 0.5F, 0.5F);
	}

	@Override
	public ResourceLocation getTextureLocation(T entity)
	{
		return TEXTURE;
	}
}