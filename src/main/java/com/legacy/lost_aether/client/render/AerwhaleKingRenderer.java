package com.legacy.lost_aether.client.render;

import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.client.LCRenderRefs;
import com.legacy.lost_aether.client.models.AerwhaleKingModel;
import com.legacy.lost_aether.client.render.layer.AerwhaleKingGlowLayer;
import com.legacy.lost_aether.entity.AerwhaleKingEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class AerwhaleKingRenderer<T extends AerwhaleKingEntity> extends MobRenderer<T, AerwhaleKingModel<T>>
{
	private static final ResourceLocation TEXTURE = LostContentMod.locate("textures/entity/aerwhale_king/aerwhale_king.png");

	public AerwhaleKingRenderer(EntityRendererProvider.Context context)
	{
		super(context, new AerwhaleKingModel<>(context.bakeLayer(LCRenderRefs.AERWHALE_KING)), 2.5F);
		this.addLayer(new AerwhaleKingGlowLayer<>(this));
	}

	@Override
	protected void scale(T entityIn, PoseStack matrixStackIn, float partialTicks)
	{
		float scale = 2.5F; // 2,5F
		matrixStackIn.scale(scale, scale, scale);
		matrixStackIn.translate(0, -0.25D, 0);
	}

	@Override
	public void render(T entityIn, float entityYaw, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn)
	{
		/*if (entityIn.getStunned())
			RenderSystem.color3f(1.0F, 1.0F, 0.5F);*/

		super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
	}

	@Override
	protected void setupRotations(T entityLiving, PoseStack stack, float ageInTicks, float rotationYaw, float partialTicks)
	{
		super.setupRotations(entityLiving, stack, ageInTicks, rotationYaw, partialTicks);
	}

	@Override
	protected boolean isShaking(T pEntity)
	{
		return super.isShaking(pEntity);
	}

	@Override
	public Vec3 getRenderOffset(T pEntity, float pPartialTicks)
	{
		/*var vec = pEntity.getEntityData().get(AerwhaleKingEntity.CRASH_POS);
		
		if (vec != BlockPos.ZERO)
			return Vec3.atBottomCenterOf(vec).subtract(pEntity.position());*/

		return super.getRenderOffset(pEntity, pPartialTicks);
	}

	@Override
	public ResourceLocation getTextureLocation(T entity)
	{
		return TEXTURE;
	}
}