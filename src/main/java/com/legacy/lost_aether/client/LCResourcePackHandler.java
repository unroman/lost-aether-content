package com.legacy.lost_aether.client;

import com.legacy.lost_aether.LostContentMod;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.server.packs.PackType;
import net.minecraft.server.packs.repository.Pack;
import net.minecraft.server.packs.repository.PackSource;
import net.minecraftforge.event.AddPackFindersEvent;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.forgespi.locating.IModFile;
import net.minecraftforge.resource.PathPackResources;

public class LCResourcePackHandler
{
	public static void packRegistry(AddPackFindersEvent event)
	{
		if (event.getPackType() == PackType.CLIENT_RESOURCES)
			register(event, Component.literal("Aether: Lost Content Classic"), "classic_pack");
	}

	private static void register(AddPackFindersEvent event, MutableComponent name, String folder)
	{
		event.addRepositorySource((consumer) ->
		{
			IModFile file = ModList.get().getModFileById(LostContentMod.MODID).getFile();
			PathPackResources pack = new PathPackResources(LostContentMod.find(folder), true, file.findResource("assets/" + LostContentMod.MODID + "/" + folder));

			consumer.accept(Pack.readMetaAndCreate(LostContentMod.find(folder), name, false, (b) -> pack, PackType.CLIENT_RESOURCES, Pack.Position.TOP, PackSource.BUILT_IN));

			/*this looks to do nothing but is needed since otherwise there's an eclipse warning*/
			pack.close();
		});
	}
}
