package com.legacy.lost_aether.event;

import java.util.List;
import java.util.Optional;

import com.aetherteam.aether.api.AetherMoaTypes;
import com.aetherteam.aether.client.AetherSoundEvents;
import com.aetherteam.aether.effect.AetherEffects;
import com.aetherteam.aether.entity.passive.FlyingCow;
import com.aetherteam.aether.entity.passive.Moa;
import com.aetherteam.aether.entity.passive.MountableAnimal;
import com.aetherteam.aether.entity.passive.Phyg;
import com.aetherteam.aether.item.miscellaneous.MoaEggItem;
import com.aetherteam.aether.util.EquipmentUtil;
import com.legacy.lost_aether.LostContentConfig;
import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.capability.CapabilityProvider;
import com.legacy.lost_aether.capability.entity.ILCMoa;
import com.legacy.lost_aether.capability.entity.LCMoa;
import com.legacy.lost_aether.capability.entity.WingedAnimalCap;
import com.legacy.lost_aether.capability.player.ILCPlayer;
import com.legacy.lost_aether.capability.player.LCPlayer;
import com.legacy.lost_aether.data.LCTags;
import com.legacy.lost_aether.network.PacketHandler;
import com.legacy.lost_aether.network.SetWingTypePacket;
import com.legacy.lost_aether.registry.LCItems;
import com.legacy.lost_aether.registry.LCMoaTypes;
import com.legacy.lost_aether.registry.LCSounds;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction.Axis;
import net.minecraft.core.Holder;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.world.Container;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntitySelector;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.Projectile;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.ToolActions;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.PlayLevelSoundEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingTickEvent;
import net.minecraftforge.event.entity.living.MobEffectEvent;
import net.minecraftforge.event.entity.living.ShieldBlockEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.level.BlockEvent;
import net.minecraftforge.eventbus.api.Event.Result;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.registries.ForgeRegistries;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotResult;

public class LCEvents
{
	/*@SubscribeEvent
	public static void onMissingMappings(MissingMappingsEvent event)
	{
		if (event.getKey().equals(Registries.BLOCK))
		{
			event.getMappings(Registries.BLOCK, LostContentMod.MODID).forEach(m ->
			{
				if (m.getKey().getPath().equals("gale_stone_stairs"))
					m.remap(LCBlocks.gale_stairs);
	
				if (m.getKey().getPath().equals("gale_stone_slab"))
					m.remap(LCBlocks.gale_slab);
	
				if (m.getKey().getPath().equals("gale_stone_wall"))
					m.remap(LCBlocks.gale_wall);
			});
		}
	}*/

	public static ItemEntity spawnAtLocation(ItemEntity itementity, Entity dropper)
	{
		ItemStack stack = itementity.getItem();
		if (!stack.isEmpty() && stack.getItem() instanceof MoaEggItem && dropper instanceof Moa moa && moa.getRandom().nextFloat() < 0.8F && (moa.getMoaType() == AetherMoaTypes.BLUE.get() || moa.getMoaType() == LCMoaTypes.ORANGE.get()))

		{
			boolean isBlue = moa.getMoaType() == AetherMoaTypes.BLUE.get();

			List<Moa> list = moa.level.<Moa>getEntitiesOfClass(Moa.class, moa.getBoundingBox().inflate(7.0D, 3.0D, 7.0D), EntitySelector.LIVING_ENTITY_STILL_ALIVE.and(EntitySelector.NO_CREATIVE_OR_SPECTATOR).and(e -> e instanceof Moa m2 && m2.getMoaType() != null));

			boolean hasOther = false;

			for (Moa moas : list)
			{
				if (isBlue && moas.getMoaType() == LCMoaTypes.ORANGE.get() || !isBlue && moas.getMoaType() == AetherMoaTypes.BLUE.get())
					hasOther = true;

				if (hasOther)
				{
					ItemStack brownEgg = new ItemStack(LCItems.brown_moa_egg);
					brownEgg.setCount(stack.getCount());
					itementity.setItem(brownEgg);
					break;
				}
			}
		}

		return itementity;
	}

	@SubscribeEvent
	public static void onEntityTick(LivingTickEvent event)
	{
		var entity = event.getEntity();

		if (entity instanceof Player player)
			LCPlayer.ifPresent(player, ILCPlayer::serverTick);

		if (entity instanceof Moa moa)
			LCMoa.ifPresent(moa, ILCMoa::serverTick);
	}

	public static void onEntityTickLast(LivingEntity entity)
	{
		if (entity instanceof MountableAnimal mount && mount.getFirstPassenger()instanceof Player rider && mount.isControlledByLocalInstance())
			LostContentConfig.CLIENT.mountRotationType.get().applyRotations(mount, rider);
	}

	@SubscribeEvent
	public static void onTrack(PlayerEvent.StartTracking event)
	{
		if ((event.getTarget() instanceof FlyingCow || event.getTarget() instanceof Phyg) && event.getEntity()instanceof ServerPlayer sp)
			WingedAnimalCap.ifPresent((Animal) event.getTarget(), (winged) -> PacketHandler.sendToClient(new SetWingTypePacket(event.getTarget().getId(), winged.getWingType()), sp));
	}

	@SubscribeEvent
	public static void onCapabilityAttached(AttachCapabilitiesEvent<Entity> event)
	{
		if ((event.getObject() instanceof FlyingCow || event.getObject() instanceof Phyg))
			event.addCapability(LostContentMod.locate("winged_animal"), new CapabilityProvider<>(new WingedAnimalCap((Animal) event.getObject())));

		if (event.getObject()instanceof Player p)
			event.addCapability(LostContentMod.locate("player_capability"), new CapabilityProvider<>(new LCPlayer(p)));

		if (event.getObject()instanceof Moa p)
			event.addCapability(LostContentMod.locate("moa_capability"), new CapabilityProvider<>(new LCMoa(p)));
	}

	@SubscribeEvent
	public static void destroyBlock(BlockEvent.BreakEvent event)
	{
		if (!(event.getLevel()instanceof ServerLevel level))
			return;

		Player player = event.getPlayer();
		BlockState state = event.getState();
		BlockPos pos = event.getPos();

		if (!player.isCreative() && player.hasCorrectToolForDrops(state) && player.getMainHandItem().is(LCTags.Items.PHOENIX_TOOLS))
		{
			List<ItemStack> drops = Block.getDrops(state, level, pos, level.getBlockEntity(pos), player, player.getMainHandItem());
			drops.forEach(itemStack -> Block.popResource(level.getLevel(), pos, getSmeltedResult(itemStack, level.getLevel())));
			level.getLevel().setBlockAndUpdate(pos, Blocks.AIR.defaultBlockState());
			return;
		}
	}

	@SubscribeEvent
	public static void onShieldBlock(ShieldBlockEvent event)
	{
		var blocker = event.getEntity();
		DamageSource source = event.getDamageSource();

		if (blocker.getUseItem().is(LCTags.Items.AETHER_SHIELDS))
		{
			blocker.level.playSound(null, blocker.blockPosition(), SoundEvents.ZOMBIE_ATTACK_IRON_DOOR, blocker.getSoundSource(), 0.4F, 0.8F + blocker.level.random.nextFloat() * 0.4F);

			if (blocker.getUseItem().getItem() == LCItems.gravitite_shield && source.getDirectEntity()instanceof LivingEntity attacker)
			{
				attacker.knockback(1.5F, blocker.getX() - attacker.getX(), blocker.getZ() - attacker.getZ());
				attacker.setPos(attacker.getX(), attacker.getY() + 1D, attacker.getZ());
				attacker.hasImpulse = true;
			}
			else if (blocker.getUseItem().getItem() == LCItems.shield_of_emile)
			{
				if (source.getDirectEntity()instanceof Projectile proj)
				{
					Projectile newProj = (Projectile) proj.getType().create(proj.level);
					newProj.setPos(proj.position());

					CompoundTag tag = new CompoundTag();
					proj.save(tag);
					newProj.load(tag);
					proj.discard();

					Vec3 vec3d = blocker.getViewVector(1.0F);
					double x = -(blocker.getX() - (blocker.getX() + vec3d.x * 16.0D));
					double y = vec3d.y * 8;
					double z = -(blocker.getZ() - (blocker.getZ() + vec3d.z * 16.0D));

					newProj.shoot(x, y, z, 1.25F, 0.0F);
					blocker.level.addFreshEntity(newProj);

					event.setCanceled(true);
				}
			}
		}

	}

	@SubscribeEvent
	public static void onLivingAttack(LivingAttackEvent event)
	{
		if (event.getEntity()instanceof Player player && !player.level.isClientSide && !player.isCreative())
		{
			DamageSource source = event.getSource();
			boolean isShielding = !player.getUseItem().isEmpty() && player.getUseItem().canPerformAction(ToolActions.SHIELD_BLOCK);

			getAccessory(player, LCItems.sentry_shield).ifPresent((slot) ->
			{
				if (source.is(DamageTypeTags.IS_EXPLOSION))
				{
					slot.stack().hurtAndBreak(1, player, p ->
					{
					});
					event.setCanceled(true);
				}
				else if (event.getAmount() > 0 && player.getRandom().nextBoolean() && !isShielding)
				{
					player.level.explode(player, player.getX(), player.getY(), player.getZ(), 2, Level.ExplosionInteraction.NONE);
					slot.stack().hurtAndBreak(1, player, p ->
					{
					});
				}
			});

			getAccessory(player, LCItems.flaming_gemstone).ifPresent((slot) ->
			{
				if (event.getAmount() > 0 && source.getDirectEntity()instanceof LivingEntity attacker && !attacker.isOnFire() && !attacker.fireImmune() && (player.getUseItem() == null || player.getUseItem() != null && !player.getUseItem().canPerformAction(ToolActions.SHIELD_BLOCK)))
				{
					player.level.playSound(null, player.blockPosition(), SoundEvents.ZOMBIE_INFECT, player.getSoundSource(), 1.0F, 1.0F);
					attacker.setSecondsOnFire(3);

					if (player.level instanceof ServerLevel sl)
					{
						var width = player.getBbWidth() / 2;
						var height = player.getBbHeight() * 0.3F;
						sl.sendParticles(ParticleTypes.FLAME, player.getX(), player.getY() + height, player.getZ(), 60, width, height, width, 0.3F);
					}

					slot.stack().hurtAndBreak(1, player, p -> p.broadcastBreakEvent(EquipmentSlot.MAINHAND));
				}
			});

			getAccessory(player, LCItems.invincibility_gem).ifPresent((slot) ->
			{
				if (!player.hasEffect(MobEffects.INVISIBILITY) && !isShielding && player.getRandom().nextFloat() < 0.1F)
				{
					player.addEffect(new MobEffectInstance(MobEffects.INVISIBILITY, 160, 0, true, true, true));
					player.level.playSound(null, player.blockPosition(), SoundEvents.ILLUSIONER_MIRROR_MOVE, player.getSoundSource(), 1.0F, 1.0F);

					if (player.level instanceof ServerLevel sl)
					{
						var width = player.getBbWidth() / 2;
						var height = player.getBbHeight() * 0.3F;
						sl.sendParticles(ParticleTypes.POOF, player.getX(), player.getY() + height, player.getZ(), 40, width, height, width, 0.1F);
					}

					slot.stack().hurtAndBreak(1, player, p ->
					{
					});

					event.setCanceled(true);
				}
			});
		}
		if (event.getSource().getDirectEntity()instanceof Player player)
		{
			getAccessory(player, LCItems.power_gloves).ifPresent((slot) ->
			{
				if (player.getMainHandItem().isEmpty() && EquipmentUtil.isFullStrength(player))
				{
					player.playSound(SoundEvents.PLAYER_ATTACK_KNOCKBACK, 1.0F, 1.0F);
					player.playSound(SoundEvents.FIREWORK_ROCKET_BLAST, 1F, 0.7F + (player.getRandom().nextFloat() * 0.2F));
					event.getEntity().knockback(1.5F, player.getX() - event.getEntity().getX(), player.getZ() - event.getEntity().getZ());

					if (event.getEntity()instanceof Player p)
						p.hurtMarked = true;

					slot.stack().hurtAndBreak(1, player, p ->
					{
					});
				}
			});
		}

	}

	@SubscribeEvent
	public static void onLivingDamage(LivingDamageEvent event)
	{
		DamageSource source = event.getSource();

		if (source.getDirectEntity()instanceof LivingEntity attacker && EquipmentUtil.isFullStrength(attacker) && attacker.getMainHandItem().is(LCTags.Items.PHOENIX_TOOLS))
		{
			int time = 5;
			int fireAspect = EnchantmentHelper.getFireAspect(attacker);

			if (fireAspect > 0)
				time += (fireAspect * 2);

			event.getEntity().setSecondsOnFire(time);
		}

		if (source == event.getEntity().damageSources().flyIntoWall() && event.getEntity().getItemBySlot(EquipmentSlot.HEAD).getItem() == LCItems.swetty_mask)
			event.setAmount(0);
	}

	@SubscribeEvent
	public static void onLivingJump(LivingEvent.LivingJumpEvent event)
	{
		if (event.getEntity()instanceof Moa moa)
		{
			if (moa.getMoaType() == LCMoaTypes.BROWN.get() && moa.isVehicle())
				moa.setDeltaMovement(moa.getDeltaMovement().with(Axis.Y, 1.2F));
		}
	}

	@SubscribeEvent
	public static void onPotionApply(MobEffectEvent.Applicable event)
	{
		if (event.getEffectInstance().getEffect() == AetherEffects.INEBRIATION.get() && event.getEntity()instanceof Moa moa && moa.getMoaType() == LCMoaTypes.BROWN.get())
			event.setResult(Result.DENY);
	}

	/*@SubscribeEvent
	public static void onSpawn(LivingSpawnEvent event)
	{
	}
	
	@SubscribeEvent
	public void onLootLoad(LootTableLoadEvent event)
	{
	}*/

	@SubscribeEvent
	public static void onPlaySound(PlayLevelSoundEvent event)
	{
		if (LostContentConfig.WORLD.mutationAnimalSounds.get() && event.getSound() != null)
		{
			var sound = event.getSound().get();

			SoundEvent ns = null;

			if (sound == AetherSoundEvents.ENTITY_FLYING_COW_AMBIENT.get())
				ns = LCSounds.ENTITY_FLYING_COW_IDLE;

			if (sound == AetherSoundEvents.ENTITY_FLYING_COW_HURT.get())
				ns = LCSounds.ENTITY_FLYING_COW_HURT;

			if (sound == AetherSoundEvents.ENTITY_FLYING_COW_DEATH.get())
				ns = LCSounds.ENTITY_FLYING_COW_DEATH;

			if (sound == AetherSoundEvents.ENTITY_PHYG_AMBIENT.get())
				ns = LCSounds.ENTITY_PHYG_IDLE;

			if (sound == AetherSoundEvents.ENTITY_PHYG_HURT.get())
				ns = LCSounds.ENTITY_PHYG_HURT;

			if (sound == AetherSoundEvents.ENTITY_PHYG_DEATH.get())
				ns = LCSounds.ENTITY_PHYG_DEATH;

			if (sound == AetherSoundEvents.ENTITY_SHEEPUFF_AMBIENT.get())
				ns = LCSounds.ENTITY_SHEEPUFF_IDLE;

			if (sound == AetherSoundEvents.ENTITY_SHEEPUFF_HURT.get())
				ns = LCSounds.ENTITY_SHEEPUFF_HURT;

			if (sound == AetherSoundEvents.ENTITY_SHEEPUFF_DEATH.get())
				ns = LCSounds.ENTITY_SHEEPUFF_DEATH;

			if (ns != null)
			{
				Optional<Holder<SoundEvent>> holder = ForgeRegistries.SOUND_EVENTS.getHolder(ns);

				if (holder.isPresent())
					event.setSound(holder.get());
			}
		}
	}

	private static ItemStack getSmeltedResult(ItemStack stack, Level level)
	{
		ItemStack output = stack;
		Container inventory = new SimpleContainer(stack);
		output = level.getRecipeManager().getRecipeFor(RecipeType.SMELTING, inventory, level).map((furnaceRecipe) -> furnaceRecipe.assemble(inventory, level.registryAccess())).orElse(stack);
		output.setCount(stack.getCount());
		return output;
	}

	public static Optional<SlotResult> getAccessory(Player player, Item item)
	{
		return CuriosApi.getCuriosHelper().findFirstCurio(player, item);
	}
}