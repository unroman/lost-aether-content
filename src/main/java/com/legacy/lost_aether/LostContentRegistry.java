package com.legacy.lost_aether;

import com.aetherteam.aether.api.AetherMoaTypes;
import com.legacy.lost_aether.capability.entity.ILCMoa;
import com.legacy.lost_aether.capability.entity.IWingedAnimal;
import com.legacy.lost_aether.data.loot_modifiers.AddCrystalSaplingsModifier;
import com.legacy.lost_aether.data.loot_modifiers.AddHolidaySaplingsModifier;
import com.legacy.lost_aether.registry.LCBlocks;
import com.legacy.lost_aether.registry.LCEntityTypes;
import com.legacy.lost_aether.registry.LCItems;
import com.legacy.lost_aether.registry.LCMoaTypes;
import com.legacy.lost_aether.registry.LCSounds;

import net.minecraft.core.registries.Registries;
import net.minecraftforge.common.capabilities.RegisterCapabilitiesEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegisterEvent;

public class LostContentRegistry
{
	@SubscribeEvent
	public static void onRegistry(RegisterEvent event)
	{
		// possible items might need to be moved down in priority for parenting
		if (event.getRegistryKey().equals(Registries.ENTITY_TYPE))
			LCEntityTypes.init(event);
		else if (event.getRegistryKey().equals(Registries.ITEM))
			LCItems.init(event);
		else if (event.getRegistryKey().equals(Registries.BLOCK))
			LCBlocks.init(event);
		else if (event.getRegistryKey().equals(Registries.SOUND_EVENT))
			LCSounds.init();
		else if (event.getRegistryKey().equals(Registries.FEATURE))
		{
			/*LCFeatures.init(event);
			LCConfiguredFeatures.init();
			LCFeaturePlacements.init();
			
			LCConfiguredCarvers.init();*/
		}
		else if (event.getRegistryKey().equals(AetherMoaTypes.MOA_TYPE_REGISTRY_KEY))
			LCMoaTypes.init(event);
		else if (event.getRegistryKey().equals(Registries.PARTICLE_TYPE))
			com.legacy.lost_aether.registry.LCParticles.init(event);
		else if (event.getRegistryKey().equals(ForgeRegistries.Keys.GLOBAL_LOOT_MODIFIER_SERIALIZERS))
		{
			event.register(ForgeRegistries.Keys.GLOBAL_LOOT_MODIFIER_SERIALIZERS, LostContentMod.locate("add_crystal_saplings"), () -> AddCrystalSaplingsModifier.CODEC);
			event.register(ForgeRegistries.Keys.GLOBAL_LOOT_MODIFIER_SERIALIZERS, LostContentMod.locate("add_holiday_saplings"), () -> AddHolidaySaplingsModifier.CODEC);
		}
	}
	
	@SubscribeEvent
	public static void registerCapabilities(final RegisterCapabilitiesEvent event)
	{
		event.register(IWingedAnimal.class);
		event.register(ILCMoa.class);
	}
}