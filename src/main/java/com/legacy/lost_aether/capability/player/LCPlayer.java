package com.legacy.lost_aether.capability.player;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import org.joml.Vector3f;

import com.aetherteam.aether.Aether;
import com.aetherteam.aether.client.AetherSoundEvents;
import com.aetherteam.aether.client.event.hooks.DimensionClientHooks;
import com.legacy.lost_aether.event.LCEvents;
import com.legacy.lost_aether.network.PacketHandler;
import com.legacy.lost_aether.network.SyncPlayerPacket;
import com.legacy.lost_aether.registry.LCItems;

import net.minecraft.client.renderer.FogRenderer.FogMode;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.util.Mth;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.ViewportEvent;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.FakePlayer;

public class LCPlayer implements ILCPlayer
{
	private static final ResourceLocation AETHER_EFFECTS = new ResourceLocation(Aether.MODID, "the_aether");
	private static final Supplier<Vector3f> KING_FOG_TINT = () -> new Vector3f(99F, 123F, 152F).div(255F); // 637b98
	private Player player;

	private float fogScale, fogScaleO;
	private boolean fogActive;

	private int fogTime;
	private int capeRiseTime;

	public LCPlayer()
	{
	}

	public LCPlayer(Player player)
	{
		super();
		this.player = player;
	}

	@Override
	public Capability<ILCPlayer> getDefaultInstance()
	{
		return ILCPlayer.INSTANCE;
	}

	@Nullable
	public static ILCPlayer get(Player player)
	{
		return LCPlayer.getIfPresent(player, lcPlayer -> lcPlayer, () -> null);
	}

	public static <E extends Player> void ifPresent(E player, Consumer<ILCPlayer> action)
	{
		if (player != null && !(player instanceof FakePlayer))
		{
			Optional<ILCPlayer> optional = player.getCapability(ILCPlayer.INSTANCE).resolve();
			if (optional.isPresent())
				action.accept(optional.get());
		}
	}

	public static <E extends Player, R> R getIfPresent(E player, Function<ILCPlayer, R> action, Supplier<R> elseSupplier)
	{
		if (player != null && !(player instanceof FakePlayer))
		{
			Optional<ILCPlayer> optional = player.getCapability(ILCPlayer.INSTANCE).resolve();
			if (optional.isPresent())
				return action.apply(optional.get());
		}
		return elseSupplier.get();
	}

	@Override
	public void serverTick()
	{
		if (this.player != null && this.player instanceof ServerPlayer player && !player.level.isClientSide())
		{
			if (this.fogTime > 0)
				this.setFogTime(this.fogTime - 1);

			if (this.capeRiseTime > 0)
			{
				if (this.capeRiseTime == 1)
					player.addEffect(new MobEffectInstance(MobEffects.SLOW_FALLING, 8 * 20, 0, true, true, true));

				this.setRiseTime(this.capeRiseTime - 1);

				if (player.level instanceof ServerLevel sl)
				{
					var width = player.getBbWidth() / 2;
					var height = player.getBbHeight() * 0.3F;
					sl.sendParticles(ParticleTypes.SMALL_FLAME, player.getX(), player.getY() + height, player.getZ(), 3, width, height, width, 0.1F);
					sl.sendParticles(ParticleTypes.HAPPY_VILLAGER, player.getX(), player.getY() + height, player.getZ(), 1, width, height, width, 0.1F);
				}
			}

			if (!player.isCreative() && !player.isShiftKeyDown() && !player.isFallFlying())
			{
				if (player.getDeltaMovement().y() < -1.6D && this.player.level.dimensionType().effectsLocation().equals(AETHER_EFFECTS) && player.getY() < 10)
				{
					LCEvents.getAccessory(player, LCItems.phoenix_cape).ifPresent((slot) ->
					{
						if (this.getRiseTime() <= 0 && !player.hasEffect(MobEffects.SLOW_FALLING) && !player.hasEffect(MobEffects.LEVITATION))
						{
							player.addEffect(new MobEffectInstance(MobEffects.LEVITATION, 5 * 20, 10, true, true, true));
							player.level.playSound(null, player.blockPosition(), AetherSoundEvents.ITEM_HAMMER_OF_KINGBDOGZ_SHOOT.get(), player.getSoundSource(), 1.0F, 1.0F);

							Vec3 vec = player.getDeltaMovement();
							player.setDeltaMovement(vec.x, 1, vec.z);
							player.hurtMarked = true;
							if (player.level instanceof ServerLevel sl)
							{
								var width = player.getBbWidth() / 2;
								var height = player.getBbHeight() * 0.3F;
								sl.sendParticles(ParticleTypes.FLAME, player.getX(), player.getY() + height, player.getZ(), 40, width, height, width, 0.1F);
							}

							slot.stack().hurtAndBreak(1, player, p ->
							{
							});

							this.setRiseTime(5 * 20);
						}
					});
				}
			}
		}
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void clientTick()
	{
		if (this.player != null && this.player instanceof net.minecraft.client.player.LocalPlayer player && player.level.isClientSide())
		{
			boolean showFog = this.getFogTime() > 0;

			if (!showFog && this.fogScale == this.fogScaleO && this.fogScale <= 0.0F)
				fogActive = false;

			this.fogScaleO = this.fogScale;
			if (showFog)
			{
				this.fogActive = true;
				this.fogScale = Mth.approach(this.fogScale, 1.0F, 0.02F);
			}
			else
				this.fogScale = Mth.approach(this.fogScale, 0.0F, 0.005F);

			if (this.capeRiseTime > 0)
			{
				for (int i = 0; i < 4; ++i)
					player.level.addParticle(ParticleTypes.FLAME, this.player.getX() + this.player.getRandom().nextGaussian() * 0.4F, this.player.getY(), this.player.getZ() + this.player.getRandom().nextGaussian() * 0.4F, this.player.getRandom().nextGaussian() * 0.2F, -0.3F, this.player.getRandom().nextGaussian() * 0.2F);
			}
		}
	}

	@Override
	public void renderFog(ViewportEvent.RenderFog event)
	{
		if (this.fogActive)
			event.setCanceled(true);
		else
			return;

		boolean terrain = event.getMode() == FogMode.FOG_TERRAIN;
		if (terrain)
		{
			// re-account for the Aether's base fog modifications
			Float renderNearFog = DimensionClientHooks.renderNearFog(event.getCamera(), event.getMode(), event.getFarPlaneDistance());
			if (renderNearFog != null)
				event.setNearPlaneDistance(renderNearFog);

			float scale = Mth.lerp((float) event.getPartialTick(), this.fogScaleO, this.fogScale);

			event.scaleFarPlaneDistance(1 - (terrain ? 0.95F : 0.1F) * scale);
			event.scaleNearPlaneDistance(1 - (terrain ? 1.05F : 0.05F) * scale);
		}
	}

	@Override
	public void modifyFogColor(ViewportEvent.ComputeFogColor event)
	{
		float scale = (1.0F * Mth.lerp((float) event.getPartialTick(), this.fogScaleO, this.fogScale));

		Vector3f base = new Vector3f(event.getRed(), event.getGreen(), event.getBlue());
		Vector3f biome = KING_FOG_TINT.get();
		Vector3f last = base.mul(1.0F - scale).add(biome.mul(scale));

		event.setRed(last.x);
		event.setGreen(last.y);
		event.setBlue(last.z);
	}

	@Override
	public Vec3 modifyAetherSkyColor(Vec3 pPos, float pPartialTick, Vec3 value)
	{
		if (this.fogActive)
		{
			float scale = 1.0F * (Mth.lerp(pPartialTick, this.fogScaleO, this.fogScale));

			Vector3f base = new Vector3f((float) value.x(), (float) value.y(), (float) value.z());
			Vector3f biome = KING_FOG_TINT.get();
			Vector3f last = base.mul(1.0F - scale).add(biome.mul(scale));

			return new Vec3(last.x, last.y, last.z);
		}

		return null;
	}

	public static int rgbToDecimal(int red, int green, int blue)
	{
		return ((red & 0x0ff) << 16) | ((green & 0x0ff) << 8) | (blue & 0x0ff);
	}

	private static final String FOG_TIME_KEY = "WhaleFogTime";
	private static final String PHOENIX_RISE_KEY = "PhoenixRiseTime";

	@Override
	public CompoundTag writeAdditional(CompoundTag nbt)
	{
		nbt.putInt(FOG_TIME_KEY, this.getFogTime());
		nbt.putInt(PHOENIX_RISE_KEY, this.getRiseTime());

		return nbt;
	}

	@Override
	public void read(CompoundTag nbt)
	{
		this.setFogTime(nbt.getInt(FOG_TIME_KEY));
		this.setRiseTime(nbt.getInt(PHOENIX_RISE_KEY));
	}

	@Override
	public void syncDataToClient()
	{
		if (getPlayer()instanceof ServerPlayer s)
			PacketHandler.sendToClient(new SyncPlayerPacket(this.writeAdditional(new CompoundTag())), s);
	}

	@Override
	public Player getPlayer()
	{
		return this.player;
	}

	@Override
	public int getFogTime()
	{
		return this.fogTime;
	}

	@Override
	public void setFogTime(int value)
	{
		this.fogTime = value;
		this.syncDataToClient();
	}

	@Override
	public int getRiseTime()
	{
		return this.capeRiseTime;
	}

	@Override
	public void setRiseTime(int value)
	{
		this.capeRiseTime = value;
		this.syncDataToClient();
	}
}
