package com.legacy.lost_aether.capability;

import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.Lazy;
import net.minecraftforge.common.util.LazyOptional;

public class CapabilityProvider<C extends IPersistentCapability<C>> implements ICapabilitySerializable<CompoundTag>
{
	private final LazyOptional<C> capabilityHandler;
	private final Lazy<Capability<C>> instance;

	public CapabilityProvider(C cap)
	{
		this.capabilityHandler = LazyOptional.of(() -> cap);
		this.instance = cap::getDefaultInstance;
	}

	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side)
	{
		return cap == this.instance.get() ? this.capabilityHandler.cast() : LazyOptional.empty();
	}

	@Override
	public void deserializeNBT(CompoundTag compound)
	{
		this.capabilityHandler.orElse(null).read(compound);
	}

	@Override
	public CompoundTag serializeNBT()
	{
		CompoundTag compound = new CompoundTag();
		this.capabilityHandler.orElse(null).writeAdditional(compound);
		return compound;
	}
}
