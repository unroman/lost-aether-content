package com.legacy.lost_aether.capability.entity;

import com.legacy.lost_aether.capability.IPersistentCapability;
import com.legacy.lost_aether.capability.entity.WingedAnimalCap.WingType;

import net.minecraft.world.entity.animal.Animal;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;

public interface IWingedAnimal extends IPersistentCapability<IWingedAnimal>
{
	public static final Capability<IWingedAnimal> INSTANCE = CapabilityManager.get(new CapabilityToken<>()
	{
	});

	byte getWingType();

	void setWingType(byte id);

	Animal getAnimal();

	void setWingType(WingType type);
	
	boolean shouldDisplayWings();
}
