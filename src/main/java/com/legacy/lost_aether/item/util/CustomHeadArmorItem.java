package com.legacy.lost_aether.item.util;

import java.util.List;
import java.util.UUID;

import javax.annotation.Nullable;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import com.legacy.lost_aether.registry.LCItems;

import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.stats.Stats;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.Equipable;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.EnchantmentCategory;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.DispenserBlock;

public class CustomHeadArmorItem extends Item implements Equipable
{
	private static final UUID ARMOR_MODIFIER_HEAD = UUID.fromString("2AD3F246-FEE1-4E67-B886-69FD380BB150"); // UUID from ArmorItem: Index 3
	private final ArmorMaterial material;
	private final Multimap<Attribute, AttributeModifier> defaultModifiers;

	public CustomHeadArmorItem(ArmorMaterial material, Properties properties)
	{
		super(properties.defaultDurability(material.getDurabilityForType(ArmorItem.Type.HELMET)));
		this.material = material;
		DispenserBlock.registerBehavior(this, ArmorItem.DISPENSE_ITEM_BEHAVIOR);
		
		ImmutableMultimap.Builder<Attribute, AttributeModifier> builder = ImmutableMultimap.builder();
		builder.put(Attributes.ARMOR, new AttributeModifier(ARMOR_MODIFIER_HEAD, "Armor modifier", this.getDefense(), AttributeModifier.Operation.ADDITION));
		builder.put(Attributes.ARMOR_TOUGHNESS, new AttributeModifier(ARMOR_MODIFIER_HEAD, "Armor toughness", this.getToughness(), AttributeModifier.Operation.ADDITION));
		if (this.getKnockbackResistance() > 0)
			builder.put(Attributes.KNOCKBACK_RESISTANCE, new AttributeModifier(ARMOR_MODIFIER_HEAD, "Armor knockback resistance", this.getKnockbackResistance(), AttributeModifier.Operation.ADDITION));

		this.defaultModifiers = builder.build();
	}

	// ---------------------------------------------------
	// This is all based on ArmorItem. This is not an ArmorItem so it can have a block model render
	@Override
	public int getEnchantmentValue()
	{
		return this.material.getEnchantmentValue();
	}

	@Override
	public boolean isValidRepairItem(ItemStack toRepair, ItemStack repairItem)
	{
		return this.material.getRepairIngredient().test(repairItem);
	}

	@Override
	public InteractionResultHolder<ItemStack> use(Level level, Player player, InteractionHand hand)
	{
		ItemStack heldItem = player.getItemInHand(hand);
		EquipmentSlot slot = Mob.getEquipmentSlotForItem(heldItem);
		ItemStack wornItem = player.getItemBySlot(slot);
		if (wornItem.isEmpty())
		{
			player.setItemSlot(slot, heldItem.copy());
			if (!level.isClientSide())
			{
				player.awardStat(Stats.ITEM_USED.get(this));
			}

			heldItem.setCount(0);
			return InteractionResultHolder.sidedSuccess(heldItem, level.isClientSide());
		}
		else
		{
			return InteractionResultHolder.fail(heldItem);
		}
	}

	@Override
	public SoundEvent getEquipSound()
	{
		return this.material.getEquipSound();
	}

	public Multimap<Attribute, AttributeModifier> getDefaultAttributeModifiers(EquipmentSlot slot)
	{
		return slot == EquipmentSlot.HEAD ? this.defaultModifiers : ImmutableMultimap.of();
	}

	public int getDefense()
	{
		return this.material.getDefenseForType(ArmorItem.Type.HELMET);
	}

	public float getToughness()
	{
		return this.material.getToughness();
	}
	// ArmorItem end
	// ---------------------------------------------------
	
	public float getKnockbackResistance()
	{
		return this.material.getKnockbackResistance();
	}

	@Override
	public EquipmentSlot getEquipmentSlot(ItemStack stack)
	{
		return EquipmentSlot.HEAD;
	}

	@Override
	public int getMaxStackSize(ItemStack stack)
	{
		return 1;
	}
	
	@Override
	public boolean isEnchantable(ItemStack pStack)
	{
		return super.isEnchantable(pStack);
	}
	
	@Override
	public boolean canApplyAtEnchantingTable(ItemStack stack, Enchantment enchantment)
	{
		EnchantmentCategory category = enchantment.category;
		// If the enchantment is for a helmet, test against iron helmet to make sure any modded enchanting logic is accounted for.
		if (category == EnchantmentCategory.ARMOR_HEAD || category == EnchantmentCategory.ARMOR)
			return category.canEnchant(Items.IRON_HELMET); 
		return category.canEnchant(stack.getItem());
	}
	
	@Override
	public void appendHoverText(ItemStack stack, @Nullable Level level, List<Component> components, TooltipFlag flag)
	{
		super.appendHoverText(stack, level, components, flag);

		if (flag.isCreative())
			components.add(LCItems.PLATINUM_DUNGEON_TOOLTIP);
	}

	@Override
	public EquipmentSlot getEquipmentSlot()
	{
		return EquipmentSlot.HEAD;
	}
}
