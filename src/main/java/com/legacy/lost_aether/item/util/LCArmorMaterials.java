package com.legacy.lost_aether.item.util;

import java.util.EnumMap;
import java.util.function.Supplier;

import com.legacy.lost_aether.LostContentMod;

import net.minecraft.Util;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraftforge.common.util.Lazy;

public enum LCArmorMaterials implements ArmorMaterial
{
	SWETTY("swetty", 5, armorValues(1, 2, 3, 1), 15, SoundEvents.ARMOR_EQUIP_LEATHER, 0.0F, 0.0F, () ->
	{
		return Ingredient.of(Items.LEATHER);
	}), AGILITY("agility", 33,armorValues(3, 6, 8, 3), 10, SoundEvents.ARMOR_EQUIP_DIAMOND, 2.0F, 0.0F, () ->
	{
		return Ingredient.of(Items.DIAMOND);
	});

	private static final EnumMap<ArmorItem.Type, Integer> HEALTH_FUNCTION_FOR_TYPE = armorValues(13, 15, 16, 11);
	private final String name;
	private final int durabilityMultiplier;
	private final EnumMap<ArmorItem.Type, Integer> slotProtections;
	private final int enchantmentValue;
	private final SoundEvent sound;
	private final float toughness;
	private final float knockbackResistance;
	private final Lazy<Ingredient> repairIngredient;

	private LCArmorMaterials(String pName, int pDurabilityMultiplier, EnumMap<ArmorItem.Type, Integer> pSlotProtections, int pEnchantmentValue, SoundEvent pSound, float pToughness, float pKnockbackResistance, Supplier<Ingredient> pRepairIngredient)
	{
		this.name = LostContentMod.find(pName);
		this.durabilityMultiplier = pDurabilityMultiplier;
		this.slotProtections = pSlotProtections;
		this.enchantmentValue = pEnchantmentValue;
		this.sound = pSound;
		this.toughness = pToughness;
		this.knockbackResistance = pKnockbackResistance;
		this.repairIngredient = Lazy.of(pRepairIngredient);
	}

	@Override
	public int getDurabilityForType(ArmorItem.Type type)
	{
		return HEALTH_FUNCTION_FOR_TYPE.get(type) * this.durabilityMultiplier;
	}

	@Override
	public int getDefenseForType(ArmorItem.Type type)
	{
		return this.slotProtections.get(type);
	}

	@Override
	public int getEnchantmentValue()
	{
		return this.enchantmentValue;
	}

	@Override
	public SoundEvent getEquipSound()
	{
		return this.sound;
	}

	@Override
	public Ingredient getRepairIngredient()
	{
		return this.repairIngredient.get();
	}

	@Override
	public String getName()
	{
		return this.name;
	}

	@Override
	public float getToughness()
	{
		return this.toughness;
	}

	@Override
	public float getKnockbackResistance()
	{
		return this.knockbackResistance;
	}
	
	public static EnumMap<ArmorItem.Type, Integer> armorValues(int boots, int legs, int chest, int helmet)
	{
		return Util.make(new EnumMap<>(ArmorItem.Type.class), (material) ->
		{
			material.put(ArmorItem.Type.BOOTS, boots);
			material.put(ArmorItem.Type.LEGGINGS, legs);
			material.put(ArmorItem.Type.CHESTPLATE, chest);
			material.put(ArmorItem.Type.HELMET, helmet);
		});
	}
}