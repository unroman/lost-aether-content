package com.legacy.lost_aether.item;

import java.util.ArrayList;
import java.util.List;

import com.aetherteam.aether.block.AetherBlocks;
import com.aetherteam.aether.item.AetherCreativeTabs;
import com.aetherteam.aether.item.AetherItems;
import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.registry.LCBlocks;
import com.legacy.lost_aether.registry.LCItems;

import net.minecraft.world.item.CreativeModeTab.TabVisibility;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ItemLike;
import net.minecraftforge.common.util.MutableHashedLinkedMap;
import net.minecraftforge.event.CreativeModeTabEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = LostContentMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class LCCreativeTabs
{
	@SubscribeEvent
	public static void modifyExisting(CreativeModeTabEvent.BuildContents event)
	{
		if (event.getTab() == AetherCreativeTabs.AETHER_EQUIPMENT_AND_UTILITIES)
		{
			insertAfter(event.getEntries(), List.of(LCItems.phoenix_sword, LCItems.phoenix_shovel, LCItems.phoenix_pickaxe, LCItems.phoenix_axe, LCItems.phoenix_hoe, LCItems.zanite_shield, LCItems.gravitite_shield, LCItems.shield_of_emile), AetherItems.VALKYRIE_HOE.get());

			insertAfter(event.getEntries(), List.of(LCItems.platinum_key), AetherItems.GOLD_DUNGEON_KEY.get());
		}

		if (event.getTab() == AetherCreativeTabs.AETHER_ARMOR_AND_ACCESSORIES)
		{
			insertAfter(event.getEntries(), List.of(LCItems.agility_boots, LCItems.swetty_mask), AetherItems.SENTRY_BOOTS.get());
			insertAfter(event.getEntries(), List.of(LCItems.phoenix_cape), AetherItems.VALKYRIE_CAPE.get());

			event.accept(LCItems.sentry_shield);
			event.accept(LCItems.invincibility_gem);
			event.accept(LCItems.flaming_gemstone);
			event.accept(LCItems.power_gloves);
		}

		if (event.getTab() == AetherCreativeTabs.AETHER_EQUIPMENT_AND_UTILITIES || event.getTab() == AetherCreativeTabs.AETHER_SPAWN_EGGS)
		{
			insertAfter(event.getEntries(), List.of(LCItems.orange_moa_egg, LCItems.brown_moa_egg), AetherItems.BLACK_MOA_EGG.get());
			insertAfter(event.getEntries(), List.of(LCItems.music_disc_legacy), AetherItems.MUSIC_DISC_ASCENDING_DAWN.get());

		}
		if (event.getTab() == AetherCreativeTabs.AETHER_NATURAL_BLOCKS)
		{
			insertAfter(event.getEntries(), List.of(LCBlocks.crystal_sapling, LCBlocks.holiday_sapling), AetherBlocks.GOLDEN_OAK_SAPLING.get().asItem());
			insertAfter(event.getEntries(), List.of(LCBlocks.pink_aercloud, LCBlocks.enchanted_pink_aercloud), AetherBlocks.GOLDEN_AERCLOUD.get().asItem());
		}

		if (event.getTab() == AetherCreativeTabs.AETHER_DUNGEON_BLOCKS)
		{
			List<ItemLike> d = new ArrayList<>();

			d.add(LCBlocks.gale_stone);
			d.add(LCBlocks.locked_gale_stone);
			d.add(LCBlocks.trapped_gale_stone);
			d.add(LCBlocks.boss_doorway_gale_stone);
			d.add(LCBlocks.treasure_doorway_gale_stone);
			d.add(LCBlocks.gale_stairs);
			d.add(LCBlocks.gale_slab);
			d.add(LCBlocks.gale_wall);

			d.add(LCBlocks.light_gale_stone);
			d.add(LCBlocks.locked_light_gale_stone);
			d.add(LCBlocks.trapped_light_gale_stone);
			d.add(LCBlocks.boss_doorway_light_gale_stone);
			d.add(LCBlocks.treasure_doorway_light_gale_stone);
			/*d.add(LCBlocks.light_gale_stone_stairs);
			d.add(LCBlocks.light_gale_stone_slab);
			d.add(LCBlocks.light_gale_stone_wall);*/
			d.add(LCBlocks.gale_pillar);
			d.add(LCBlocks.songstone);

			insertAfter(event.getEntries(), d, AetherBlocks.TREASURE_DOORWAY_LIGHT_HELLFIRE_STONE.get().asItem());
		}
	}

	protected static void insertAfter(MutableHashedLinkedMap<ItemStack, TabVisibility> entries, List<ItemLike> items, ItemLike target)
	{
		ItemStack currentStack = null;

		for (var e : entries)
		{
			if (e.getKey().getItem() == target)
			{
				currentStack = e.getKey();
				break;
			}
		}

		for (var item : items)
			entries.putAfter(currentStack, currentStack = item.asItem().getDefaultInstance(), TabVisibility.PARENT_AND_SEARCH_TABS);
	}
}
