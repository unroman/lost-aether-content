package com.legacy.lost_aether.network;

import java.util.function.Supplier;

import com.legacy.lost_aether.capability.entity.IWingedAnimal;
import com.legacy.lost_aether.capability.entity.WingedAnimalCap;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.animal.Animal;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.network.NetworkEvent;

public class SetWingTypePacket
{
	private final int entityId;
	private final byte wingType;

	public SetWingTypePacket(int entityIdIn, byte id)
	{
		this.entityId = entityIdIn;
		this.wingType = id;
	}

	public static void encoder(SetWingTypePacket packet, FriendlyByteBuf buff)
	{
		buff.writeInt(packet.entityId);
		buff.writeByte(packet.wingType);
	}

	public static SetWingTypePacket decoder(FriendlyByteBuf buff)
	{
		return new SetWingTypePacket(buff.readInt(), buff.readByte());
	}

	public static void handler(SetWingTypePacket packet, Supplier<NetworkEvent.Context> context)
	{
		context.get().enqueueWork(() -> DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () -> handlePacket(packet)));
		context.get().setPacketHandled(true);
	}

	@SuppressWarnings("resource")
	@OnlyIn(Dist.CLIENT)
	private static void handlePacket(SetWingTypePacket packet)
	{		
		IWingedAnimal cap;
		if (net.minecraft.client.Minecraft.getInstance().level.getEntity(packet.entityId)instanceof Animal animal && (cap = WingedAnimalCap.get(animal)) != null)
			cap.setWingType(packet.wingType);
	}
}
