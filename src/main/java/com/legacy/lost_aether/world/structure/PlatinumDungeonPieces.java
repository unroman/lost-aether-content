package com.legacy.lost_aether.world.structure;

import java.util.ArrayList;

import com.aetherteam.aether.api.BossRoomTracker;
import com.aetherteam.aether.block.AetherBlocks;
import com.aetherteam.aether.blockentity.TreasureChestBlockEntity;
import com.legacy.lost_aether.LostContentMod;
import com.legacy.lost_aether.data.LCLootProv;
import com.legacy.lost_aether.entity.AerwhaleKingEntity;
import com.legacy.lost_aether.registry.LCBlocks;
import com.legacy.lost_aether.registry.LCEntityTypes;
import com.legacy.lost_aether.registry.LCItems;
import com.legacy.lost_aether.registry.LCStructures;
import com.legacy.structure_gel.api.structure.GelTemplateStructurePiece;
import com.legacy.structure_gel.api.structure.processor.RemoveGelStructureProcessor;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.StructureManager;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.ChestBlock;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.ChestBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.RandomState;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.levelgen.structure.StructurePiece;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceSerializationContext;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePiecesBuilder;
import net.minecraft.world.level.levelgen.structure.templatesystem.BlockIgnoreProcessor;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructurePlaceSettings;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplateManager;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;

public class PlatinumDungeonPieces
{
	private static final ResourceLocation TOWER = locatePiece("tower");
	private static final ResourceLocation GROUND = locatePiece("ground");

	public static void assemble(StructureTemplateManager templateManager, BlockPos pos, Rotation rotation, StructurePiecesBuilder builder, RandomSource random, RandomState randomState)
	{
		/*builder.addPiece(new PlatinumDungeonPieces.StoneFillPiece(pos.offset(-20, -21, -20), new BlockPos(20, 20, 20), 0));*/

		builder.addPiece(new PlatinumDungeonPieces.Piece(templateManager, GROUND, pos, rotation));

		builder.addPiece(new PlatinumDungeonPieces.Piece(templateManager, TOWER, pos.offset(new BlockPos(9, 21, 9)/*.rotate(rotation)*/), rotation));
	}

	static ResourceLocation locatePiece(String location)
	{
		return LostContentMod.locate("platinum_dungeon/" + location);
	}

	public static class Piece extends GelTemplateStructurePiece
	{
		public Piece(StructureTemplateManager structureManager, ResourceLocation name, BlockPos pos, Rotation rotation)
		{
			super(LCStructures.PLATINUM_DUNGEON.getPieceType("main").get(), 0, structureManager, name, Piece.getPlacementSettings(structureManager, name, pos, rotation), pos);
			this.rotation = rotation;
		}

		public Piece(StructurePieceSerializationContext context, CompoundTag nbt)
		{
			super(LCStructures.PLATINUM_DUNGEON.getPieceType("main").get(), nbt, context.structureTemplateManager(), name -> Piece.getPlacementSettings(context.structureTemplateManager(), name, new BlockPos(nbt.getInt("TPX"), nbt.getInt("TPY"), nbt.getInt("TPZ")), Rotation.valueOf(nbt.getString("Rot"))));
		}

		private static StructurePlaceSettings getPlacementSettings(StructureTemplateManager structureManager, ResourceLocation name, BlockPos pos, Rotation rotation)
		{
			Vec3i sizePos = structureManager.get(name).get().getSize();
			BlockPos centerPos = new BlockPos(sizePos.getX() / 2, 0, sizePos.getZ() / 2);

			StructurePlaceSettings placementSettings = new StructurePlaceSettings().setKeepLiquids(false).setRotationPivot(centerPos).setRotation(rotation).setMirror(Mirror.NONE);
			placementSettings.addProcessor(BlockIgnoreProcessor.STRUCTURE_BLOCK);
			placementSettings.addProcessor(RemoveGelStructureProcessor.INSTANCE);

			return placementSettings;
		}

		@Override
		public BlockState modifyState(ServerLevelAccessor level, RandomSource rand, BlockPos pos, BlockState originalState)
		{
			/*if (originalState.getBlock() == AetherBlocks.SKYROOT_FENCE.get())
				return IModifyState.mergeStates(LCBlocks.gale_wall.defaultBlockState(), originalState);
			
			if (originalState.getBlock() == AetherBlocks.SKYROOT_PLANKS.get())
				return IModifyState.mergeStates(LCBlocks.gale_stone.defaultBlockState(), originalState);
			
			if (originalState.getBlock() == AetherBlocks.SKYROOT_STAIRS.get())
				return IModifyState.mergeStates(LCBlocks.gale_stairs.defaultBlockState(), originalState);
			
			if (originalState.getBlock() == AetherBlocks.SKYROOT_SLAB.get())
				return IModifyState.mergeStates(LCBlocks.gale_slab.defaultBlockState(), originalState);*/

			// Replace Gale Stone randomly with the light variant
			if (originalState.getBlock() == LCBlocks.locked_gale_stone && rand.nextFloat() < 0.05F)
				return LCBlocks.locked_light_gale_stone.defaultBlockState();

			if (rand.nextFloat() < 0.005F/* && level.isEmptyBlock(pos.above())*/)
			{
				if (originalState.getBlock() == LCBlocks.locked_gale_stone)
					return LCBlocks.trapped_gale_stone.defaultBlockState();
				else if (originalState.getBlock() == LCBlocks.locked_light_gale_stone)
					return LCBlocks.trapped_light_gale_stone.defaultBlockState();
			}

			return originalState;
		}

		// create
		@Override
		public void postProcess(WorldGenLevel level, StructureManager structureManager, ChunkGenerator chunkGeneratorIn, RandomSource randomIn, BoundingBox bounds, ChunkPos chunkPosIn, BlockPos pos)
		{
			super.postProcess(level, structureManager, chunkGeneratorIn, randomIn, bounds, chunkPosIn, pos);
		}

		@Override
		protected void handleDataMarker(String function, BlockPos pos, ServerLevelAccessor level, RandomSource rand, BoundingBox sbb)
		{
			if (function.contains("boss"))
			{
				AerwhaleKingEntity entity = new AerwhaleKingEntity(LCEntityTypes.AERWHALE_KING, level.getLevel());
				entity.setPos((double) pos.getX() - 16.0D, (double) pos.getY() + 12.0D, (double) pos.getZ());

				entity.finalizeSpawn(level, level.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, null, null);

				entity.setDungeon(new BossRoomTracker<AerwhaleKingEntity>(entity, Vec3.atBottomCenterOf(pos), new AABB(pos).inflate(20, 10, 20).move(0, 8, 0), new ArrayList<>()));
				entity.setYHeadRot(180);

				level.addFreshEntity(entity);
			}
			else if (function.contains("platinum_chest"))
			{
				level.setBlock(pos, Blocks.AIR.defaultBlockState(), 3);

				if (level.getBlockEntity(pos.below())instanceof TreasureChestBlockEntity be)
				{
					be.setLootTable(LCLootProv.PLATINUM_TREASURE_LOOT, rand.nextLong());
					be.setKind(LCItems.PLATINUM_KEY_TYPE);
				}
			}
			else if (function.contains("loot_chest"))
			{
				BlockPos blockpos = pos.below();
				BlockEntity tile = level.getBlockEntity(blockpos);

				if (tile instanceof ChestBlockEntity chest)
				{
					if (rand.nextFloat() < 0.2F)
					{
						if (level.getBlockState(blockpos).hasProperty(ChestBlock.FACING))
							level.setBlock(blockpos, AetherBlocks.CHEST_MIMIC.get().defaultBlockState().setValue(ChestBlock.FACING, level.getBlockState(blockpos).getValue(ChestBlock.FACING)), 3);
					}
					else
						chest.setLootTable(LCLootProv.PLATINUM_DUNGEON_LOOT, rand.nextLong());
				}

				level.setBlock(pos, Blocks.AIR.defaultBlockState(), 3);
			}
		}
	}

	public static class StoneFillPiece extends StructurePiece
	{
		protected StoneFillPiece(BlockPos pos, BlockPos size, int componentType)
		{
			super(LCStructures.PLATINUM_DUNGEON.getPieceType("stone_fill").get(), componentType, BoundingBox.fromCorners(pos, pos.offset(size)));
		}

		public StoneFillPiece(StructurePieceSerializationContext context, CompoundTag nbt)
		{
			super(LCStructures.PLATINUM_DUNGEON.getPieceType("stone_fill").get(), nbt);
		}

		@Override
		protected void addAdditionalSaveData(StructurePieceSerializationContext context, CompoundTag nbt)
		{
		}

		@Override
		public void postProcess(WorldGenLevel world, StructureManager structureManager, ChunkGenerator chunkGen, RandomSource rand, BoundingBox bounds, ChunkPos chunkPos, BlockPos pos)
		{
			this.generateBox(world, bounds, this.boundingBox.minX(), this.boundingBox.minY(), this.boundingBox.minZ(), this.boundingBox.maxX(), this.boundingBox.maxY(), this.boundingBox.maxZ(), AetherBlocks.AETHER_DIRT.get().defaultBlockState(), AetherBlocks.AETHER_DIRT.get().defaultBlockState(), false);
		}
	}
}
