package com.legacy.lost_aether.world.structure;

import java.util.Optional;

import com.legacy.lost_aether.registry.LCStructures;
import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.minecraft.world.level.levelgen.structure.StructureType;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePiecesBuilder;

public class PlatinumDungeonStructure extends Structure
{
	public static final Codec<PlatinumDungeonStructure> CODEC = Structure.simpleCodec(PlatinumDungeonStructure::new);

	public PlatinumDungeonStructure(Structure.StructureSettings settings)
	{
		super(settings);
	}

	private void generatePieces(StructurePiecesBuilder builder, GenerationContext context, int y)
	{
		ChunkPos chunkPos = context.chunkPos();
		Rotation rotation = Rotation.values()[context.random().nextInt(Rotation.values().length)];
		PlatinumDungeonPieces.assemble(context.structureTemplateManager(), new BlockPos(chunkPos.x * 16 + 8, y, chunkPos.z * 16 + 8), rotation, builder, context.random(), context.randomState());
	}

	@Override
	public Optional<GenerationStub> findGenerationPoint(GenerationContext context)
	{
		ChunkPos chunkpos = context.chunkPos();
		BlockPos blockpos = new BlockPos(chunkpos.getMinBlockX(), 130 + context.random().nextInt(20), chunkpos.getMinBlockZ());
		return Optional.of(new GenerationStub(blockpos, (piecesBuilder) -> generatePieces(piecesBuilder, context, blockpos.getY())));

	}

	@Override
	public StructureType<?> type()
	{
		return LCStructures.PLATINUM_DUNGEON.getType();
	}
}