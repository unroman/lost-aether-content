package com.legacy.lost_aether.mixin;

import javax.annotation.Nonnull;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.aetherteam.aether.client.renderer.entity.layers.QuadrupedWingsLayer;
import com.aetherteam.aether.entity.passive.WingedAnimal;
import com.legacy.lost_aether.capability.entity.IWingedAnimal;
import com.legacy.lost_aether.capability.entity.WingedAnimalCap;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.model.QuadrupedModel;
import net.minecraft.client.renderer.MultiBufferSource;

@Mixin(QuadrupedWingsLayer.class)
public class QuadrupedWingsLayerMixin<T extends WingedAnimal, M extends QuadrupedModel<T>>
{
	@Inject(at = @At("HEAD"), method = "render", cancellable = true, remap = false)
	private void render(@Nonnull PoseStack poseStack, @Nonnull MultiBufferSource buffer, int packedLight, T entity, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, CallbackInfo callback)
	{
		IWingedAnimal cap = WingedAnimalCap.get(entity);
		if (cap != null && cap.shouldDisplayWings())
			callback.cancel();
	}
}
