package com.legacy.lost_aether.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.aetherteam.aether.blockentity.IncubatorBlockEntity;
import com.legacy.lost_aether.data.LCTags;

import net.minecraft.world.level.Level;

@Mixin(IncubatorBlockEntity.class)
public class IncubatorBlockEntityMixin
{
	@Inject(at = @At("RETURN"), method = "getTotalIncubationTime", cancellable = true, remap = false)
	private static void getTotalCookTime(Level pLevel, IncubatorBlockEntity pBlockEntity, CallbackInfoReturnable<Integer> callback)
	{
		if (pLevel.getBlockState(pBlockEntity.getBlockPos().below()).is(LCTags.Blocks.INCUBATOR_ENHANCER))
			callback.setReturnValue((int) ((float) callback.getReturnValue() * 0.7F));

	}
}