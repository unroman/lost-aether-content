package com.legacy.lost_aether.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.aetherteam.aether.blockentity.AbstractAetherFurnaceBlockEntity;
import com.aetherteam.aether.blockentity.AltarBlockEntity;
import com.aetherteam.aether.blockentity.FreezerBlockEntity;
import com.legacy.lost_aether.data.LCTags;

import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.AbstractFurnaceBlockEntity;
import net.minecraft.world.level.block.state.BlockState;

@Mixin(AbstractFurnaceBlockEntity.class)
public class AbstractFurnaceBlockEntityMixin
{	
	@Inject(at = @At("RETURN"), method = "getTotalCookTime", cancellable = true)
	private static void getTotalCookTime(Level pLevel, AbstractFurnaceBlockEntity pBlockEntity, CallbackInfoReturnable<Integer> callback)
	{
		if (!(pBlockEntity instanceof AbstractAetherFurnaceBlockEntity))
			return;

		boolean fast = false;
		BlockState block = pLevel.getBlockState(pBlockEntity.getBlockPos().below());

		if (pBlockEntity instanceof AltarBlockEntity && block.is(LCTags.Blocks.ALTAR_ENHANCER))
			fast = true;

		if (pBlockEntity instanceof FreezerBlockEntity && block.is(LCTags.Blocks.FREEZER_ENHANCER))
			fast = true;

		if (fast)
			callback.setReturnValue((int) ((float) callback.getReturnValue() * 0.7F));
	}
}
