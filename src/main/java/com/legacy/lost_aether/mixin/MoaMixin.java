package com.legacy.lost_aether.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.aetherteam.aether.entity.passive.Moa;
import com.legacy.lost_aether.capability.entity.LCMoa;

@Mixin(Moa.class)
public class MoaMixin
{
	@Inject(at = @At("RETURN"), method = "getFlyingSpeed", cancellable = true)
	private void getFlyingSpeed(CallbackInfoReturnable<Float> callback)
	{
		LCMoa.ifPresent((Moa) (Object) this, (lc) -> lc.modifySpeed(callback));
	}
}
