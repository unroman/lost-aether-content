package com.legacy.lost_aether.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.aetherteam.aether.client.renderer.level.AetherSkyRenderEffects;
import com.legacy.lost_aether.capability.player.LCPlayer;

import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.world.phys.Vec3;

@Mixin(AetherSkyRenderEffects.class)
public class AetherSkyRenderEffectsMixin
{
	@SuppressWarnings("resource")
	@Inject(at = @At("RETURN"), method = "getSkyColor", cancellable = true, remap = false)
	private void getSkyColor(ClientLevel world, Vec3 pPos, float pPartialTick, CallbackInfoReturnable<Vec3> callback)
	{
		Vec3 colorMod = LCPlayer.getIfPresent(Minecraft.getInstance().player, p -> p.modifyAetherSkyColor(pPos, pPartialTick, callback.getReturnValue()), () -> null);

		if (colorMod != null)
			callback.setReturnValue(colorMod);
	}
}
