package com.legacy.lost_aether.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyVariable;

import com.legacy.lost_aether.event.LCEvents;

import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.item.ItemEntity;

@Mixin(Entity.class)
public class EntityMixin
{
	@ModifyVariable(at = @At("STORE"), method = "spawnAtLocation(Lnet/minecraft/world/item/ItemStack;F)Lnet/minecraft/world/entity/item/ItemEntity;", ordinal = 0)
	private ItemEntity modify$itementity(ItemEntity itementity)
	{
		return LCEvents.spawnAtLocation(itementity, (Entity) (Object) this);
	}
}
