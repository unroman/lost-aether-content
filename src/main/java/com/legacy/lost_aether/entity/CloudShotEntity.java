package com.legacy.lost_aether.entity;

import com.aetherteam.aether.entity.BossMob;
import com.legacy.lost_aether.registry.LCEntityTypes;
import com.legacy.lost_aether.registry.LCSounds;

import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.world.entity.AreaEffectCloud;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.projectile.ThrowableProjectile;
import net.minecraft.world.item.alchemy.Potions;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.network.NetworkHooks;

public class CloudShotEntity extends ThrowableProjectile
{
	public CloudShotEntity(EntityType<? extends CloudShotEntity> pEntityType, Level pLevel)
	{
		super(pEntityType, pLevel);
	}

	public CloudShotEntity(Level pLevel, LivingEntity pShooter)
	{
		super(LCEntityTypes.CLOUD_SHOT, pShooter, pLevel);
	}

	public CloudShotEntity(Level pLevel, double pX, double pY, double pZ)
	{
		super(LCEntityTypes.CLOUD_SHOT, pX, pY, pZ, pLevel);
	}

	@Override
	public Packet<ClientGamePacketListener> getAddEntityPacket()
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}

	@Override
	protected void onHitBlock(BlockHitResult pResult)
	{
		super.onHitBlock(pResult);

		if (!this.level.isClientSide)
		{
			this.makeAreaOfEffectCloud(false);
			this.discard();
		}
	}

	@Override
	protected void onHitEntity(EntityHitResult pResult)
	{
		super.onHitEntity(pResult);

		// this.makeAreaOfEffectCloud(false);
	}

	private void makeAreaOfEffectCloud(boolean atPos)
	{
		Entity entity = this.getOwner();

		double height = atPos ? this.getY() : Math.max((entity instanceof BossMob<?> boss && boss.getDungeon() != null ? boss.getDungeon().originCoordinates().y() : this.getY()), this.level.getHeight(Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, (int) this.getX(), (int) this.getZ()));

		AreaEffectCloud cloud = new AreaEffectCloud(this.level, this.getX(), height, this.getZ());

		if (entity instanceof LivingEntity)
			cloud.setOwner((LivingEntity) entity);

		cloud.setRadius(3.0F);
		cloud.setRadiusOnUse(-0.5F);
		cloud.setWaitTime(10);
		cloud.setRadiusPerTick(-cloud.getRadius() / (float) cloud.getDuration());
		cloud.setPotion(Potions.HARMING);
		cloud.setParticle(ParticleTypes.CLOUD);

		cloud.playSound(LCSounds.ENTITY_CLOUD_SHOT_PUFF, 1.5F, 1.0F);
		this.level.addFreshEntity(cloud);
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.level.isClientSide && this.random.nextFloat() < 0.8)
		{
			float r = 0.1F;
			float v = 0.02F;
			Vec3 pos = this.position().add(0, this.getBbHeight() / 2, 0);
			this.level.addParticle(ParticleTypes.CLOUD, pos.x + this.random.nextGaussian() * r, pos.y + this.random.nextGaussian() * r, pos.z + this.random.nextGaussian() * r, this.random.nextGaussian() * v, this.random.nextGaussian() * v, this.random.nextGaussian() * v);
		}
	}

	@Override
	protected void defineSynchedData()
	{
	}

}
