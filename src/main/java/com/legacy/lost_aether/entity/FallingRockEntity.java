package com.legacy.lost_aether.entity;

import java.util.List;

import com.aetherteam.aether.entity.projectile.crystal.AbstractCrystal;
import com.google.common.collect.Lists;
import com.legacy.lost_aether.registry.LCBlocks;
import com.legacy.lost_aether.registry.LCEntityTypes;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.util.Mth;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.phys.Vec3;

public class FallingRockEntity extends AbstractCrystal
{
	public LivingEntity shootingEntity;

	public float sinage[];

	public FallingRockEntity(EntityType<? extends FallingRockEntity> type, Level worldIn)
	{
		super(type, worldIn);

		// this.setSize(0.9F, 0.9F);
		this.sinage = new float[3];

		for (int i = 0; i < 3; i++)
		{
			this.sinage[i] = this.random.nextFloat() * 6F;
		}
	}

	public FallingRockEntity(Level worldIn, LivingEntity shooter)
	{
		this(LCEntityTypes.FALLING_ROCK, worldIn);

		this.shootingEntity = shooter;

		this.setNoGravity(false);
	}

	@Override
	public void tick()
	{
		super.tick();
		BlockPos blockpos = this.blockPosition();

		if (!this.level.isClientSide)
		{
			if (this.onGround || this.tickCount > 200 || this.blockPosition().getY() <= 0 || !this.level.getBlockState(this.blockPosition()).isAir())
			{
				if (this.tickCount > 5)
				{
					this.level.levelEvent(2001, blockpos, Block.getId(LCBlocks.gale_stone.defaultBlockState()));
					this.discard();
				}
			}

			if (this.getDeltaMovement().y() != 0)
			{
				List<Entity> list = Lists.newArrayList(this.level.getEntities(this, this.getBoundingBox()));
				for (Entity entity : list)
				{
					this.damage(entity, 0.0F);
				}
			}
		}

		if (this.isInWaterOrRain())
			this.clearFire();
	}

	protected void tickMovement()
	{
		Vec3 vec3 = this.getDeltaMovement();
		double d2 = this.getX() + vec3.x;
		double d0 = this.getY() + vec3.y;
		double d1 = this.getZ() + vec3.z;
		this.updateRotation();
		float f;

		if (this.isInWater())
		{
			for (int i = 0; i < 4; ++i)
			{
				float f1 = 0.25F;
				this.level.addParticle(ParticleTypes.BUBBLE, d2 - vec3.x * f1, d0 - vec3.y * f1, d1 - vec3.z * f1, vec3.x, vec3.y, vec3.z);
			}

			f = 0.8F;
		}
		else
			f = 0.99F;

		this.setDeltaMovement(vec3.scale((double) f));
		if (!this.isNoGravity())
		{
			Vec3 vec31 = this.getDeltaMovement();
			this.setDeltaMovement(vec31.x, vec31.y - 0.03D, vec31.z);
		}

		this.setPos(d2, d0, d1);

	}

	@Override
	public boolean causeFallDamage(float distance, float damageMultiplier, DamageSource source)
	{
		if (Mth.ceil(distance - 1.0F) > 0)
		{
			for (Entity entity : Lists.newArrayList(this.level.getEntities(this, this.getBoundingBox())))
				this.damage(entity, 1.0F);
		}

		return false;
	}

	@Override
	public boolean hurt(DamageSource source, float amount)
	{
		return false;
	}

	private void damage(Entity hitEntityIn, float amp)
	{
		LivingEntity spawner = this.shootingEntity;

		if (hitEntityIn.isAlive() && !hitEntityIn.isInvulnerable() && hitEntityIn != spawner)
		{
			if (spawner == null)
			{
				hitEntityIn.hurt(this.damageSources().fallingBlock(this), 6.0F + amp);
				/*hitEntityIn.hurt(new DamageSource(LostContentMod.MODID + ".rock_fall").bypassArmor(), 6.0F + amp);*/
			}

			else
			{
				if (spawner.isAlliedTo(hitEntityIn))
				{
					return;
				}

				hitEntityIn.hurt(this.damageSources().fallingBlock(spawner)/*new EntityDamageSource(LostContentMod.MODID + ".rock_fall_mob", spawner).bypassArmor()*/, 6.0F + amp);
			}
		}
	}

	@Override
	protected ParticleOptions getExplosionParticle()
	{
		return ParticleTypes.CLOUD;
	}
}
