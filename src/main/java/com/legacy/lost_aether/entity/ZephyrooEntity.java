package com.legacy.lost_aether.entity;

import com.aetherteam.aether.entity.passive.MountableAnimal;
import com.aetherteam.aether.item.AetherItems;

import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.AgeableMob;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.FollowParentGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.PanicGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.TemptGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;

public class ZephyrooEntity extends MountableAnimal
{
	public int maxJumps;
	public int jumpsRemaining;

	public ZephyrooEntity(EntityType<? extends ZephyrooEntity> type, Level world)
	{
		super(type, world);

		this.jumpsRemaining = 0;
		this.maxJumps = 0;
		//this.maxUpStep = 1.0F;

		this.noCulling = true;
		/*this.canJumpMidAir = true;*/
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new FloatGoal(this));
		this.goalSelector.addGoal(1, new PanicGoal(this, 1.25D));
		// this.goalSelector.addGoal(2, new MateGoal(this, 1.0D));
		this.goalSelector.addGoal(3, new TemptGoal(this, 1.25D, Ingredient.of(AetherItems.BLUE_BERRY.get(), AetherItems.ENCHANTED_BERRY.get()), false));
		this.goalSelector.addGoal(4, new LookAtPlayerGoal(this, Player.class, 6.0F));
		this.goalSelector.addGoal(5, new RandomLookAroundGoal(this));
		this.goalSelector.addGoal(5, new FollowParentGoal(this, 1.1D));
		this.goalSelector.addGoal(6, new WaterAvoidingRandomStrollGoal(this, 1.0D));
	}

	public static AttributeSupplier.Builder registerAttributes()
	{
		return Mob.createMobAttributes().add(Attributes.MAX_HEALTH, 40.0D).add(Attributes.MOVEMENT_SPEED, 0.4D);
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.isOnGround())
		{
			if (this.zza != 0.0F)
				this.jumpFromGround();

			this.jumpsRemaining = this.maxJumps;
		}

		this.fallDistance = 0;
	}

	@Override
	public double getPassengersRidingOffset()
	{
		return 1.2D;
	}

	@Override
	protected void jumpFromGround()
	{
		super.jumpFromGround();
	}

	@Override
	public boolean causeFallDamage(float distance, float damageMultiplier, DamageSource source)
	{
		return false;
	}

	@Override
	public double getMountJumpStrength()
	{
		return 1.0D;
	}

	@Override
	public void travel(Vec3 vec)
	{
		/*Entity entity = this.getPassengers().isEmpty() ? null : this.getPassengers().get(0);
		
		if (entity instanceof Player player)
		{
			this.yRotO = player.getYRot();
			this.xRotO = player.getXRot();
			this.yHeadRot = this.getYRot();
			this.yBodyRot = this.getYRot();
		
			float strafe = player.xxa;
			float vertical = player.yya;
			float forward = player.zza;
		
			if (forward <= 0.0F)
			{
				forward *= 0.25F;
			}
		
			if (forward != 0.0F && this.onGround)
			{
				this.jumpFromGround();
			}
		
			this.setDeltaMovement(this.getDeltaMovement().multiply(0.35F, 1, 0.35F));
		
			this.maxUpStep = 1.0F;
		
			if (!this.level.isClientSide)
			{
				this.flyingSpeed = this.getSpeed() * 0.6F;
				super.travel(new Vec3(strafe, vertical, forward));
			}
		
			if (this.onGround)
			{
				this.jumpPower = 0.0F;
				this.setMountJumping(false);
			}
		
			this.animationSpeedOld = this.animationSpeed;
			double d0 = this.getX() - this.xo;
			double d1 = this.getZ() - this.zo;
			float f4 = Mth.sqrt(d0 * d0 + d1 * d1) * 4.0F;
		
			if (f4 > 1.0F)
			{
				f4 = 1.0F;
			}
		
			this.animationSpeed += (f4 - this.animationSpeed) * 0.4F;
			this.animationPosition += this.animationSpeed;
		}
		else
		{
			this.maxUpStep = 0.5F;
			this.flyingSpeed = 0.02F;
			super.travel(vec);
		}*/

		super.travel(vec);
	}

	@Override
	public boolean isFood(ItemStack stack)
	{
		Item item = stack.getItem();
		return item == AetherItems.BLUE_BERRY.get() || item == AetherItems.ENCHANTED_BERRY.get();
	}

	@Override
	public InteractionResult mobInteract(Player player, InteractionHand hand)
	{
		ItemStack itemstack = player.getItemInHand(hand);

		if (!super.mobInteract(player, hand).consumesAction() && itemstack.getItem() != Items.NAME_TAG && !this.isFood(itemstack) && !this.isBaby())
		{
			player.startRiding(this);
			return InteractionResult.SUCCESS;
		}

		return super.mobInteract(player, hand);
	}

	@Override
	public AgeableMob getBreedOffspring(ServerLevel world, AgeableMob entityageable)
	{
		return null; // LCEntityTypes.ZEPHYROO.create(world);
	}

	@Override
	public boolean canJump()
	{
		return false;
	}

	@Override
	protected void dropEquipment()
	{
		super.dropEquipment();
	}

	@Override
	public boolean isSaddled()
	{
		return true;
	}
}