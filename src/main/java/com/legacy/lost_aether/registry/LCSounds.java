package com.legacy.lost_aether.registry;

import com.legacy.lost_aether.LostContentMod;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraftforge.registries.ForgeRegistries;

public class LCSounds
{
	public static final SoundEvent ENTITY_AERWHALE_KING_IDLE = create("entity.aerwhale_king.idle");
	public static final SoundEvent ENTITY_AERWHALE_KING_HURT = create("entity.aerwhale_king.hurt");
	public static final SoundEvent ENTITY_AERWHALE_KING_DEATH = create("entity.aerwhale_king.death");
	public static final SoundEvent ENTITY_AERWHALE_KING_CHARGE = create("entity.aerwhale_king.charge");
	public static final SoundEvent ENTITY_AERWHALE_KING_SHOOT = create("entity.aerwhale_king.shoot");
	public static final SoundEvent ENTITY_AERWHALE_KING_SPOUT_FOG = create("entity.aerwhale_king.spout_fog");
	public static final SoundEvent ENTITY_AERWHALE_KING_IDLE_DISTANT = create("entity.aerwhale_king.idle_distant");

	public static final SoundEvent ENTITY_CLOUD_SHOT_PUFF = create("entity.cloud_shot.puff");

	public static final SoundEvent ENTITY_FLYING_COW_IDLE = create("entity.flying_cow.idle_override");
	public static final SoundEvent ENTITY_FLYING_COW_HURT = create("entity.flying_cow.hurt_override");
	public static final SoundEvent ENTITY_FLYING_COW_DEATH = create("entity.flying_cow.death_override");

	public static final SoundEvent ENTITY_PHYG_IDLE = create("entity.phyg.idle_override");
	public static final SoundEvent ENTITY_PHYG_HURT = create("entity.phyg.hurt_override");
	public static final SoundEvent ENTITY_PHYG_DEATH = create("entity.phyg.death_override");

	public static final SoundEvent ENTITY_SHEEPUFF_IDLE = create("entity.sheepuff.idle_override");
	public static final SoundEvent ENTITY_SHEEPUFF_HURT = create("entity.sheepuff.hurt_override");
	public static final SoundEvent ENTITY_SHEEPUFF_DEATH = create("entity.sheepuff.death_override");

	public static final SoundEvent BLOCK_SONGSTONE_USE = create("block.songstone.use");

	public static final SoundEvent RECORD_LEGACY = create("record.legacy");

	public static void init()
	{
	}

	private static SoundEvent create(String name)
	{
		ResourceLocation location = LostContentMod.locate(name);
		SoundEvent sound = SoundEvent.createVariableRangeEvent(location);
		ForgeRegistries.SOUND_EVENTS.register(location, sound);
		return sound;
	}

}